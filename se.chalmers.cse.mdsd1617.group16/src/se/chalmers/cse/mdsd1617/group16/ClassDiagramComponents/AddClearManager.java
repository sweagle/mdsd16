/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Add Clear Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage#getAddClearManager()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface AddClearManager extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false" numOfBedsRequired="true" numOfBedsOrdered="false" additionalFeaturesRequired="true" additionalFeaturesOrdered="false"
	 * @generated
	 */
	RoomType addRoomType(String name, double price, int numOfBeds, String additionalFeatures);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	Room addRoom(int roomNumber, RoomType roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearRooms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearRoomTypes();

} // AddClearManager
