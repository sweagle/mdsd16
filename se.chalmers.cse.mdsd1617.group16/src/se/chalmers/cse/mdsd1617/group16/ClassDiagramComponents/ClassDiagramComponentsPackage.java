/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsFactory
 * @model kind="package"
 * @generated
 */
public interface ClassDiagramComponentsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ClassDiagramComponents";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group16/ClassDiagramComponents.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClassDiagramComponentsPackage eINSTANCE = se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getIHotelCustomerProvides()
	 * @generated
	 */
	int IHOTEL_CUSTOMER_PROVIDES = 0;

	/**
	 * The number of structural features of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = 2;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = 3;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = 4;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 5;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = 6;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 7;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = 8;

	/**
	 * The number of operations of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = 9;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.FreeRoomTypesDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getFreeRoomTypesDTO()
	 * @generated
	 */
	int FREE_ROOM_TYPES_DTO = 1;

	/**
	 * The feature id for the '<em><b>Room Type Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Num Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_BEDS = 1;

	/**
	 * The feature id for the '<em><b>Price Per Night</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = 2;

	/**
	 * The feature id for the '<em><b>Num Free Rooms</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = 3;

	/**
	 * The number of structural features of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getIHotelStartupProvides()
	 * @generated
	 */
	int IHOTEL_STARTUP_PROVIDES = 2;

	/**
	 * The number of structural features of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES___STARTUP__INT = 0;

	/**
	 * The number of operations of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.HotelCustomerProvidesImpl <em>Hotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.HotelCustomerProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getHotelCustomerProvides()
	 * @generated
	 */
	int HOTEL_CUSTOMER_PROVIDES = 3;

	/**
	 * The number of structural features of the '<em>Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The number of operations of the '<em>Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.HotelStartupProvidesImpl <em>Hotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.HotelStartupProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getHotelStartupProvides()
	 * @generated
	 */
	int HOTEL_STARTUP_PROVIDES = 4;

	/**
	 * The number of structural features of the '<em>Hotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES_FEATURE_COUNT = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES___STARTUP__INT = IHOTEL_STARTUP_PROVIDES___STARTUP__INT;

	/**
	 * The number of operations of the '<em>Hotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES_OPERATION_COUNT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ExternalSupplierSystemImpl <em>External Supplier System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ExternalSupplierSystemImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getExternalSupplierSystem()
	 * @generated
	 */
	int EXTERNAL_SUPPLIER_SYSTEM = 5;

	/**
	 * The number of structural features of the '<em>External Supplier System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM_FEATURE_COUNT = HOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM___GET_FREE_ROOMS__INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM___INITIATE_BOOKING__STRING_STRING_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM___ADD_ROOM_TO_BOOKING__STRING_INT = HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM___CONFIRM_BOOKING__INT = HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM___INITIATE_CHECKOUT__INT = HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM___INITIATE_ROOM_CHECKOUT__INT_INT = HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM___CHECK_IN_ROOM__STRING_INT = HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The number of operations of the '<em>External Supplier System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM_OPERATION_COUNT = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking <em>Core Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getCoreBooking()
	 * @generated
	 */
	int CORE_BOOKING = 6;

	/**
	 * The number of structural features of the '<em>Core Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_BOOKING_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Create Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_BOOKING___CREATE_BOOKING__STRING_STRING_STRING_STRING = 0;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_BOOKING___CHECK_OUT_BOOKING__INT = 1;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_BOOKING___CHECK_IN_ROOM__STRING_INT = 2;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_BOOKING___CHECK_OUT_ROOM__INT_INT = 3;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_BOOKING___ADD_ROOM_TO_BOOKING__INT_STRING = 4;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_BOOKING___CONFIRM_BOOKING__INT = 5;

	/**
	 * The operation id for the '<em>Get Booking From Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_BOOKING___GET_BOOKING_FROM_ROOM__INT = 6;

	/**
	 * The operation id for the '<em>Get Booking From Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_BOOKING___GET_BOOKING_FROM_NAME__STRING_STRING = 7;

	/**
	 * The number of operations of the '<em>Core Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_BOOKING_OPERATION_COUNT = 8;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ReceptionistImpl <em>Receptionist</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ReceptionistImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getReceptionist()
	 * @generated
	 */
	int RECEPTIONIST = 12;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking <em>Manage Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getManageBooking()
	 * @generated
	 */
	int MANAGE_BOOKING = 13;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl <em>Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getBooking()
	 * @generated
	 */
	int BOOKING = 7;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomReservationImpl <em>Room Reservation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomReservationImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getRoomReservation()
	 * @generated
	 */
	int ROOM_RESERVATION = 8;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClearBooking <em>Clear Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClearBooking
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getClearBooking()
	 * @generated
	 */
	int CLEAR_BOOKING = 15;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingManagerImpl <em>Booking Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getBookingManager()
	 * @generated
	 */
	int BOOKING_MANAGER = 14;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 11;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomTypeImpl <em>Room Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomTypeImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getRoomType()
	 * @generated
	 */
	int ROOM_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ID = 0;

	/**
	 * The feature id for the '<em><b>Start Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__START_DATE = 1;

	/**
	 * The feature id for the '<em><b>End Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__END_DATE = 2;

	/**
	 * The feature id for the '<em><b>Amount Paid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__AMOUNT_PAID = 3;

	/**
	 * The feature id for the '<em><b>Guest Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__GUEST_NAME = 4;

	/**
	 * The feature id for the '<em><b>Reservations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__RESERVATIONS = 5;

	/**
	 * The feature id for the '<em><b>Confirmed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__CONFIRMED = 6;

	/**
	 * The feature id for the '<em><b>Is Payed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__IS_PAYED = 7;

	/**
	 * The feature id for the '<em><b>Checked In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__CHECKED_IN = 8;

	/**
	 * The feature id for the '<em><b>Checked Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__CHECKED_OUT = 9;

	/**
	 * The number of structural features of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_FEATURE_COUNT = 10;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_ROOM__ROOM = 0;

	/**
	 * The operation id for the '<em>Get Total Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_TOTAL_COST = 1;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___REMOVE_ROOM__ROOMTYPE = 2;

	/**
	 * The number of operations of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_OPERATION_COUNT = 3;

	/**
	 * The feature id for the '<em><b>Room Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__ROOM_ID = 0;

	/**
	 * The feature id for the '<em><b>Booking</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__BOOKING = 1;

	/**
	 * The feature id for the '<em><b>Room Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__ROOM_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Extra Cost</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__EXTRA_COST = 3;

	/**
	 * The feature id for the '<em><b>Is Checked Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__IS_CHECKED_OUT = 4;

	/**
	 * The number of structural features of the '<em>Room Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Get Total Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___GET_TOTAL_COST = 0;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___ADD_EXTRA_COST__STRING_DOUBLE = 1;

	/**
	 * The operation id for the '<em>Check Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___CHECK_OUT = 2;

	/**
	 * The number of operations of the '<em>Room Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION_OPERATION_COUNT = 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Number Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NUMBER_OF_BEDS = 1;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__COST = 2;

	/**
	 * The feature id for the '<em><b>Additional Features</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__ADDITIONAL_FEATURES = 3;

	/**
	 * The number of structural features of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ExtraCostImpl <em>Extra Cost</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ExtraCostImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getExtraCost()
	 * @generated
	 */
	int EXTRA_COST = 10;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_COST__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_COST__COST = 1;

	/**
	 * The number of structural features of the '<em>Extra Cost</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_COST_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Extra Cost</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_COST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListFreeRooms <em>List Free Rooms</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListFreeRooms
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getListFreeRooms()
	 * @generated
	 */
	int LIST_FREE_ROOMS = 17;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms <em>List Rooms</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getListRooms()
	 * @generated
	 */
	int LIST_ROOMS = 16;

	/**
	 * The feature id for the '<em><b>Room Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_NUMBER = 0;

	/**
	 * The feature id for the '<em><b>Is Free</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__IS_FREE = 1;

	/**
	 * The feature id for the '<em><b>Is Blocked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__IS_BLOCKED = 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__TYPE = 3;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = 0;

	/**
	 * The number of structural features of the '<em>Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_FEATURE_COUNT = HOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___GET_FREE_ROOMS__INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___INITIATE_BOOKING__STRING_STRING_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___ADD_ROOM_TO_BOOKING__STRING_INT = HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CONFIRM_BOOKING__INT = HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___INITIATE_CHECKOUT__INT = HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___INITIATE_ROOM_CHECKOUT__INT_INT = HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHECK_IN_ROOM__STRING_INT = HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Change Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHANGE_DATE__INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___LIST_BOOKINGS = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___LIST_CHECK_INS__STRING_STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHECK_IN__INT = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___REMOVE_ROOM_FROM_BOOKING__INT_STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Booking Room Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___GET_BOOKING_ROOM_RESERVATIONS__BOOKING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>List Check Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___LIST_CHECK_OUT__STRING_STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CANCEL_BOOKING__INT = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___GET_BOOKING__INT = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHANGE_ROOM_TYPE__INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>List Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___LIST_OCCUPIED_ROOMS__STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 11;

	/**
	 * The number of operations of the '<em>Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_OPERATION_COUNT = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 12;

	/**
	 * The number of structural features of the '<em>Manage Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_BOOKING_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_BOOKING___LIST_BOOKINGS = 0;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_BOOKING___CHANGE_ROOM_TYPE__INT_STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_BOOKING___REMOVE_ROOM_FROM_BOOKING__INT_STRING = 2;

	/**
	 * The operation id for the '<em>Change Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_BOOKING___CHANGE_DATE__INT_STRING_STRING = 3;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_BOOKING___LIST_CHECK_INS__STRING_STRING = 4;

	/**
	 * The operation id for the '<em>List Check Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_BOOKING___LIST_CHECK_OUT__STRING_STRING = 5;

	/**
	 * The feature id for the '<em><b>Is Free</b></em>' attribute.
	*/
	
	int MANAGE_BOOKING___CHECK_IN__INT = 6;

	/**
	 * The feature id for the '<em><b>Is Blocked</b></em>' attribute.
	*/
	int MANAGE_BOOKING___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE = 7;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * 
	 * */
	int MANAGE_BOOKING___CANCEL_BOOKING__INT = 8;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * 
	 * */
	int MANAGE_BOOKING___GET_BOOKING_ROOM_RESERVATIONS__BOOKING = 9;

	/**
	 * The number of operations of the '<em>Room</em>' class.
*/
	int MANAGE_BOOKING___GET_BOOKING__INT = 10;
	int MANAGE_BOOKING_OPERATION_COUNT = 11;

	/**
	 * The number of structural features of the '<em>Clear Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_BOOKING_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Clear Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_BOOKING___CLEAR_BOOKINGS = 0;

	/**
	 * The number of operations of the '<em>Clear Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_BOOKING_OPERATION_COUNT = 1;

	/**
	 * The feature id for the '<em><b>Bookings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER__BOOKINGS = CLEAR_BOOKING_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Booking Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER_FEATURE_COUNT = CLEAR_BOOKING_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Clear Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CLEAR_BOOKINGS = CLEAR_BOOKING___CLEAR_BOOKINGS;

	/**
	 * The operation id for the '<em>Create Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CREATE_BOOKING__STRING_STRING_STRING_STRING = CLEAR_BOOKING_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_OUT_BOOKING__INT = CLEAR_BOOKING_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_IN_ROOM__STRING_INT = CLEAR_BOOKING_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_OUT_ROOM__INT_INT = CLEAR_BOOKING_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___ADD_ROOM_TO_BOOKING__INT_STRING = CLEAR_BOOKING_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CONFIRM_BOOKING__INT = CLEAR_BOOKING_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Booking From Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_BOOKING_FROM_ROOM__INT = CLEAR_BOOKING_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Booking From Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_BOOKING_FROM_NAME__STRING_STRING = CLEAR_BOOKING_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>List Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_FREE_ROOMS__STRING_STRING_INT = CLEAR_BOOKING_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>List Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_ROOMS = CLEAR_BOOKING_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>List Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_OCCUPIED_ROOMS__STRING = CLEAR_BOOKING_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_BOOKINGS = CLEAR_BOOKING_OPERATION_COUNT + 11;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHANGE_ROOM_TYPE__INT_STRING_STRING = CLEAR_BOOKING_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___REMOVE_ROOM_FROM_BOOKING__INT_STRING = CLEAR_BOOKING_OPERATION_COUNT + 13;

	/**
	 * The operation id for the '<em>Change Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHANGE_DATE__INT_STRING_STRING = CLEAR_BOOKING_OPERATION_COUNT + 14;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_CHECK_INS__STRING_STRING = CLEAR_BOOKING_OPERATION_COUNT + 15;

	/**
	 * The operation id for the '<em>List Check Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_CHECK_OUT__STRING_STRING = CLEAR_BOOKING_OPERATION_COUNT + 16;

	/**
	 * The operation id for the '<em>Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_IN__INT = CLEAR_BOOKING_OPERATION_COUNT + 17;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE = CLEAR_BOOKING_OPERATION_COUNT + 18;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CANCEL_BOOKING__INT = CLEAR_BOOKING_OPERATION_COUNT + 19;

	/**
	 * The operation id for the '<em>Get Booking Room Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_BOOKING_ROOM_RESERVATIONS__BOOKING = CLEAR_BOOKING_OPERATION_COUNT + 20;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_BOOKING__INT = CLEAR_BOOKING_OPERATION_COUNT + 21;

	/**
	 * The number of operations of the '<em>Booking Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER_OPERATION_COUNT = CLEAR_BOOKING_OPERATION_COUNT + 22;

	/**
	 * The number of structural features of the '<em>List Free Rooms</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_FREE_ROOMS_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>List Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_FREE_ROOMS___LIST_FREE_ROOMS__STRING_STRING_INT = 0;

	/**
	 * The number of operations of the '<em>List Free Rooms</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_FREE_ROOMS_OPERATION_COUNT = 1;

	/**
	 * The number of structural features of the '<em>List Rooms</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_ROOMS_FEATURE_COUNT = LIST_FREE_ROOMS_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>List Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_ROOMS___LIST_FREE_ROOMS__STRING_STRING_INT = LIST_FREE_ROOMS___LIST_FREE_ROOMS__STRING_STRING_INT;

	/**
	 * The operation id for the '<em>List Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_ROOMS___LIST_ROOMS = LIST_FREE_ROOMS_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>List Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_ROOMS___LIST_OCCUPIED_ROOMS__STRING = LIST_FREE_ROOMS_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>List Rooms</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_ROOMS_OPERATION_COUNT = LIST_FREE_ROOMS_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomManagerImpl <em>Room Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getRoomManager()
	 * @generated
	 */
	int ROOM_MANAGER = 18;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager <em>Add Clear Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getAddClearManager()
	 * @generated
	 */
	int ADD_CLEAR_MANAGER = 20;

	/**
	 * The number of structural features of the '<em>Add Clear Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_CLEAR_MANAGER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_CLEAR_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = 0;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_CLEAR_MANAGER___ADD_ROOM__INT_ROOMTYPE = 1;

	/**
	 * The operation id for the '<em>Clear Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_CLEAR_MANAGER___CLEAR_ROOMS = 2;

	/**
	 * The operation id for the '<em>Clear Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_CLEAR_MANAGER___CLEAR_ROOM_TYPES = 3;

	/**
	 * The number of operations of the '<em>Add Clear Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_CLEAR_MANAGER_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager <em>Manage Room Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getManageRoomManager()
	 * @generated
	 */
	int MANAGE_ROOM_MANAGER = 19;

	/**
	 * The number of structural features of the '<em>Manage Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER_FEATURE_COUNT = ADD_CLEAR_MANAGER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = ADD_CLEAR_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER___ADD_ROOM__INT_ROOMTYPE = ADD_CLEAR_MANAGER___ADD_ROOM__INT_ROOMTYPE;

	/**
	 * The operation id for the '<em>Clear Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER___CLEAR_ROOMS = ADD_CLEAR_MANAGER___CLEAR_ROOMS;

	/**
	 * The operation id for the '<em>Clear Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER___CLEAR_ROOM_TYPES = ADD_CLEAR_MANAGER___CLEAR_ROOM_TYPES;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER___CHANGE_ROOM_TYPE__INT_ROOMTYPE = ADD_CLEAR_MANAGER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER___REMOVE_ROOM__INT = ADD_CLEAR_MANAGER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER___BLOCK_ROOM__INT = ADD_CLEAR_MANAGER_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER___UNBLOCK_ROOM__INT = ADD_CLEAR_MANAGER_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = ADD_CLEAR_MANAGER_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER___REMOVE_ROOM_TYPE__STRING = ADD_CLEAR_MANAGER_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER___GET_ROOM_TYPE__STRING = ADD_CLEAR_MANAGER_OPERATION_COUNT + 6;

	/**
	 * The number of operations of the '<em>Manage Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGE_ROOM_MANAGER_OPERATION_COUNT = ADD_CLEAR_MANAGER_OPERATION_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER__ROOMS = MANAGE_ROOM_MANAGER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Room Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER__ROOM_TYPES = MANAGE_ROOM_MANAGER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_FEATURE_COUNT = MANAGE_ROOM_MANAGER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = MANAGE_ROOM_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___ADD_ROOM__INT_ROOMTYPE = MANAGE_ROOM_MANAGER___ADD_ROOM__INT_ROOMTYPE;

	/**
	 * The operation id for the '<em>Clear Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CLEAR_ROOMS = MANAGE_ROOM_MANAGER___CLEAR_ROOMS;

	/**
	 * The operation id for the '<em>Clear Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CLEAR_ROOM_TYPES = MANAGE_ROOM_MANAGER___CLEAR_ROOM_TYPES;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CHANGE_ROOM_TYPE__INT_ROOMTYPE = MANAGE_ROOM_MANAGER___CHANGE_ROOM_TYPE__INT_ROOMTYPE;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___REMOVE_ROOM__INT = MANAGE_ROOM_MANAGER___REMOVE_ROOM__INT;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___BLOCK_ROOM__INT = MANAGE_ROOM_MANAGER___BLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___UNBLOCK_ROOM__INT = MANAGE_ROOM_MANAGER___UNBLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = MANAGE_ROOM_MANAGER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___REMOVE_ROOM_TYPE__STRING = MANAGE_ROOM_MANAGER___REMOVE_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_ROOM_TYPE__STRING = MANAGE_ROOM_MANAGER___GET_ROOM_TYPE__STRING;

	/**
	 * The number of operations of the '<em>Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_OPERATION_COUNT = MANAGE_ROOM_MANAGER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.AdminImpl <em>Admin</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.AdminImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getAdmin()
	 * @generated
	 */
	int ADMIN = 21;

	/**
	 * The number of structural features of the '<em>Admin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___UNBLOCK_ROOM__INT = 0;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = 1;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___GET_ROOM_TYPE__STRING = 2;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___REMOVE_ROOM__INT = 3;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___CHANGE_ROOM_TYPE__INT_ROOMTYPE = 4;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___REMOVE_ROOM_TYPE__STRING = 5;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___BLOCK_ROOM__INT = 6;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___STARTUP__INT = 7;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___ADD_ROOM__INT_ROOMTYPE = 8;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = 9;

	/**
	 * The number of operations of the '<em>Admin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_OPERATION_COUNT = 10;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides <em>IHotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides
	 * @generated
	 */
	EClass getIHotelCustomerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__GetFreeRooms__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#confirmBooking(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#initiateCheckout(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#initiateRoomCheckout(int, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#checkInRoom(java.lang.String, int) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides#checkInRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__CheckInRoom__String_int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO <em>Free Room Types DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Free Room Types DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO
	 * @generated
	 */
	EClass getFreeRoomTypesDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO#getRoomTypeDescription <em>Room Type Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Type Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO#getRoomTypeDescription()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_RoomTypeDescription();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO#getNumBeds <em>Num Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO#getNumBeds()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO#getPricePerNight <em>Price Per Night</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price Per Night</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO#getPricePerNight()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_PricePerNight();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO#getNumFreeRooms <em>Num Free Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Free Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO#getNumFreeRooms()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumFreeRooms();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides <em>IHotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides
	 * @generated
	 */
	EClass getIHotelStartupProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides#startup(int) <em>Startup</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Startup</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides#startup(int)
	 * @generated
	 */
	EOperation getIHotelStartupProvides__Startup__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.HotelCustomerProvides <em>Hotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.HotelCustomerProvides
	 * @generated
	 */
	EClass getHotelCustomerProvides();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.HotelStartupProvides <em>Hotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.HotelStartupProvides
	 * @generated
	 */
	EClass getHotelStartupProvides();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ExternalSupplierSystem <em>External Supplier System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Supplier System</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ExternalSupplierSystem
	 * @generated
	 */
	EClass getExternalSupplierSystem();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking <em>Core Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Core Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking
	 * @generated
	 */
	EClass getCoreBooking();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#createBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Create Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#createBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getCoreBooking__CreateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#checkOutBooking(int) <em>Check Out Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#checkOutBooking(int)
	 * @generated
	 */
	EOperation getCoreBooking__CheckOutBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#checkInRoom(java.lang.String, int) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#checkInRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getCoreBooking__CheckInRoom__String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#checkOutRoom(int, int) <em>Check Out Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#checkOutRoom(int, int)
	 * @generated
	 */
	EOperation getCoreBooking__CheckOutRoom__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#addRoomToBooking(int, java.lang.String) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#addRoomToBooking(int, java.lang.String)
	 * @generated
	 */
	EOperation getCoreBooking__AddRoomToBooking__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#confirmBooking(int)
	 * @generated
	 */
	EOperation getCoreBooking__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#getBookingFromRoom(int) <em>Get Booking From Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking From Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#getBookingFromRoom(int)
	 * @generated
	 */
	EOperation getCoreBooking__GetBookingFromRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#getBookingFromName(java.lang.String, java.lang.String) <em>Get Booking From Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking From Name</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking#getBookingFromName(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getCoreBooking__GetBookingFromName__String_String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist <em>Receptionist</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Receptionist</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist
	 * @generated
	 */
	EClass getReceptionist();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#changeDate(int, java.lang.String, java.lang.String) <em>Change Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#changeDate(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getReceptionist__ChangeDate__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#listBookings()
	 * @generated
	 */
	EOperation getReceptionist__ListBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#listCheckIns(java.lang.String, java.lang.String) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#listCheckIns(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getReceptionist__ListCheckIns__String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#checkIn(int) <em>Check In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#checkIn(int)
	 * @generated
	 */
	EOperation getReceptionist__CheckIn__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#removeRoomFromBooking(int, java.lang.String) <em>Remove Room From Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room From Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#removeRoomFromBooking(int, java.lang.String)
	 * @generated
	 */
	EOperation getReceptionist__RemoveRoomFromBooking__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#getBookingRoomReservations(se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking) <em>Get Booking Room Reservations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking Room Reservations</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#getBookingRoomReservations(se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking)
	 * @generated
	 */
	EOperation getReceptionist__GetBookingRoomReservations__Booking();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#listCheckOut(java.lang.String, java.lang.String) <em>List Check Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Out</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#listCheckOut(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getReceptionist__ListCheckOut__String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#cancelBooking(int)
	 * @generated
	 */
	EOperation getReceptionist__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#getBooking(int) <em>Get Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#getBooking(int)
	 * @generated
	 */
	EOperation getReceptionist__GetBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#changeRoomType(int, java.lang.String, java.lang.String) <em>Change Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#changeRoomType(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getReceptionist__ChangeRoomType__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#addExtraCost(int, int, java.lang.String, double) <em>Add Extra Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#addExtraCost(int, int, java.lang.String, double)
	 * @generated
	 */
	EOperation getReceptionist__AddExtraCost__int_int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#listOccupiedRooms(java.lang.String) <em>List Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Occupied Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist#listOccupiedRooms(java.lang.String)
	 * @generated
	 */
	EOperation getReceptionist__ListOccupiedRooms__String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking <em>Manage Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Manage Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking
	 * @generated
	 */
	EClass getManageBooking();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#listBookings()
	 * @generated
	 */
	EOperation getManageBooking__ListBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#changeRoomType(int, java.lang.String, java.lang.String) <em>Change Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#changeRoomType(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getManageBooking__ChangeRoomType__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#removeRoomFromBooking(int, java.lang.String) <em>Remove Room From Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room From Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#removeRoomFromBooking(int, java.lang.String)
	 * @generated
	 */
	EOperation getManageBooking__RemoveRoomFromBooking__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#changeDate(int, java.lang.String, java.lang.String) <em>Change Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#changeDate(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getManageBooking__ChangeDate__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#listCheckIns(java.lang.String, java.lang.String) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#listCheckIns(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getManageBooking__ListCheckIns__String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#listCheckOut(java.lang.String, java.lang.String) <em>List Check Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Out</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#listCheckOut(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getManageBooking__ListCheckOut__String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#checkIn(int) <em>Check In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#checkIn(int)
	 * @generated
	 */
	EOperation getManageBooking__CheckIn__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#addExtraCost(int, int, java.lang.String, double) <em>Add Extra Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#addExtraCost(int, int, java.lang.String, double)
	 * @generated
	 */
	EOperation getManageBooking__AddExtraCost__int_int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#cancelBooking(int)
	 * @generated
	 */
	EOperation getManageBooking__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#getBookingRoomReservations(se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking) <em>Get Booking Room Reservations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking Room Reservations</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#getBookingRoomReservations(se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking)
	 * @generated
	 */
	EOperation getManageBooking__GetBookingRoomReservations__Booking();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#getBooking(int) <em>Get Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking#getBooking(int)
	 * @generated
	 */
	EOperation getManageBooking__GetBooking__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking
	 * @generated
	 */
	EClass getBooking();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getId()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_Id();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getStartDate <em>Start Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Date</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getStartDate()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_StartDate();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getEndDate <em>End Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Date</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getEndDate()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_EndDate();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getAmountPaid <em>Amount Paid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Amount Paid</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getAmountPaid()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_AmountPaid();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getGuestName <em>Guest Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Guest Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getGuestName()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_GuestName();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getReservations <em>Reservations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reservations</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getReservations()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Reservations();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#isConfirmed <em>Confirmed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Confirmed</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#isConfirmed()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_Confirmed();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#isPayed <em>Is Payed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Payed</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#isPayed()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_IsPayed();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#isCheckedIn <em>Checked In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Checked In</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#isCheckedIn()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_CheckedIn();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#isCheckedOut <em>Checked Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Checked Out</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#isCheckedOut()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_CheckedOut();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#addRoom(se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#addRoom(se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room)
	 * @generated
	 */
	EOperation getBooking__AddRoom__Room();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getTotalCost() <em>Get Total Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Total Cost</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#getTotalCost()
	 * @generated
	 */
	EOperation getBooking__GetTotalCost();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#removeRoom(se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking#removeRoom(se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType)
	 * @generated
	 */
	EOperation getBooking__RemoveRoom__RoomType();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation <em>Room Reservation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Reservation</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation
	 * @generated
	 */
	EClass getRoomReservation();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#getRoomId <em>Room Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#getRoomId()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EAttribute getRoomReservation_RoomId();

	/**
	 * Returns the meta object for the containment reference '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#getBooking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#getBooking()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EReference getRoomReservation_Booking();

	/**
	 * Returns the meta object for the containment reference '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#getRoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#getRoomType()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EReference getRoomReservation_RoomType();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#getExtraCost <em>Extra Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extra Cost</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#getExtraCost()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EReference getRoomReservation_ExtraCost();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#isCheckedOut <em>Is Checked Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Checked Out</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#isCheckedOut()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EAttribute getRoomReservation_IsCheckedOut();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#getTotalCost() <em>Get Total Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Total Cost</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#getTotalCost()
	 * @generated
	 */
	EOperation getRoomReservation__GetTotalCost();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#addExtraCost(java.lang.String, double) <em>Add Extra Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#addExtraCost(java.lang.String, double)
	 * @generated
	 */
	EOperation getRoomReservation__AddExtraCost__String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#checkOut() <em>Check Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation#checkOut()
	 * @generated
	 */
	EOperation getRoomReservation__CheckOut();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.BookingManager <em>Booking Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.BookingManager
	 * @generated
	 */
	EClass getBookingManager();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.BookingManager#getBookings <em>Bookings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bookings</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.BookingManager#getBookings()
	 * @see #getBookingManager()
	 * @generated
	 */
	EReference getBookingManager_Bookings();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClearBooking <em>Clear Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Clear Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClearBooking
	 * @generated
	 */
	EClass getClearBooking();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClearBooking#clearBookings() <em>Clear Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClearBooking#clearBookings()
	 * @generated
	 */
	EOperation getClearBooking__ClearBookings();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room#getRoomNumber <em>Room Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Number</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room#getRoomNumber()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_RoomNumber();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room#isFree <em>Is Free</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Free</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room#isFree()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_IsFree();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room#isBlocked <em>Is Blocked</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Blocked</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room#isBlocked()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_IsBlocked();

	/**
	 * Returns the meta object for the containment reference '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room#getType()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_Type();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType
	 * @generated
	 */
	EClass getRoomType();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType#getType()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Type();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType#getNumberOfBeds <em>Number Of Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType#getNumberOfBeds()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_NumberOfBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType#getCost <em>Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cost</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType#getCost()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Cost();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType#getAdditionalFeatures <em>Additional Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Additional Features</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType#getAdditionalFeatures()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_AdditionalFeatures();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ExtraCost <em>Extra Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extra Cost</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ExtraCost
	 * @generated
	 */
	EClass getExtraCost();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ExtraCost#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ExtraCost#getDescription()
	 * @see #getExtraCost()
	 * @generated
	 */
	EAttribute getExtraCost_Description();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ExtraCost#getCost <em>Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cost</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ExtraCost#getCost()
	 * @see #getExtraCost()
	 * @generated
	 */
	EAttribute getExtraCost_Cost();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager <em>Room Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager
	 * @generated
	 */
	EClass getRoomManager();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager#getRooms()
	 * @see #getRoomManager()
	 * @generated
	 */
	EReference getRoomManager_Rooms();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager#getRoomTypes <em>Room Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Room Types</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager#getRoomTypes()
	 * @see #getRoomManager()
	 * @generated
	 */
	EReference getRoomManager_RoomTypes();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms <em>List Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>List Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms
	 * @generated
	 */
	EClass getListRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms#listRooms() <em>List Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms#listRooms()
	 * @generated
	 */
	EOperation getListRooms__ListRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms#listOccupiedRooms(java.lang.String) <em>List Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Occupied Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms#listOccupiedRooms(java.lang.String)
	 * @generated
	 */
	EOperation getListRooms__ListOccupiedRooms__String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListFreeRooms <em>List Free Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>List Free Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListFreeRooms
	 * @generated
	 */
	EClass getListFreeRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListFreeRooms#listFreeRooms(java.lang.String, java.lang.String, int) <em>List Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Free Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListFreeRooms#listFreeRooms(java.lang.String, java.lang.String, int)
	 * @generated
	 */
	EOperation getListFreeRooms__ListFreeRooms__String_String_int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager <em>Manage Room Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Manage Room Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager
	 * @generated
	 */
	EClass getManageRoomManager();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#changeRoomType(int, se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType) <em>Change Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#changeRoomType(int, se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType)
	 * @generated
	 */
	EOperation getManageRoomManager__ChangeRoomType__int_RoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#removeRoom(int)
	 * @generated
	 */
	EOperation getManageRoomManager__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#blockRoom(int)
	 * @generated
	 */
	EOperation getManageRoomManager__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#unblockRoom(int)
	 * @generated
	 */
	EOperation getManageRoomManager__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#updateRoomType(java.lang.String, double, int, java.lang.String) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#updateRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getManageRoomManager__UpdateRoomType__String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getManageRoomManager__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#getRoomType(java.lang.String) <em>Get Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager#getRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getManageRoomManager__GetRoomType__String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager <em>Add Clear Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Add Clear Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager
	 * @generated
	 */
	EClass getAddClearManager();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager#addRoomType(java.lang.String, double, int, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager#addRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getAddClearManager__AddRoomType__String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager#addRoom(int, se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager#addRoom(int, se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType)
	 * @generated
	 */
	EOperation getAddClearManager__AddRoom__int_RoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager#clearRooms() <em>Clear Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager#clearRooms()
	 * @generated
	 */
	EOperation getAddClearManager__ClearRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager#clearRoomTypes() <em>Clear Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager#clearRoomTypes()
	 * @generated
	 */
	EOperation getAddClearManager__ClearRoomTypes();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin <em>Admin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Admin</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin
	 * @generated
	 */
	EClass getAdmin();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#unblockRoom(int)
	 * @generated
	 */
	EOperation getAdmin__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#updateRoomType(java.lang.String, double, int, java.lang.String) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#updateRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getAdmin__UpdateRoomType__String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#getRoomType(java.lang.String) <em>Get Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#getRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getAdmin__GetRoomType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#removeRoom(int)
	 * @generated
	 */
	EOperation getAdmin__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#changeRoomType(int, se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType) <em>Change Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#changeRoomType(int, se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType)
	 * @generated
	 */
	EOperation getAdmin__ChangeRoomType__int_RoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getAdmin__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#blockRoom(int)
	 * @generated
	 */
	EOperation getAdmin__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#startup(int) <em>Startup</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Startup</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#startup(int)
	 * @generated
	 */
	EOperation getAdmin__Startup__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#addRoom(int, se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#addRoom(int, se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType)
	 * @generated
	 */
	EOperation getAdmin__AddRoom__int_RoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#addRoomType(java.lang.String, double, int, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin#addRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getAdmin__AddRoomType__String_double_int_String();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ClassDiagramComponentsFactory getClassDiagramComponentsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getIHotelCustomerProvides()
		 * @generated
		 */
		EClass IHOTEL_CUSTOMER_PROVIDES = eINSTANCE.getIHotelCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__GetFreeRooms__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getIHotelCustomerProvides__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = eINSTANCE.getIHotelCustomerProvides__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = eINSTANCE.getIHotelCustomerProvides__InitiateCheckout__int();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = eINSTANCE.getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = eINSTANCE.getIHotelCustomerProvides__CheckInRoom__String_int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.FreeRoomTypesDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getFreeRoomTypesDTO()
		 * @generated
		 */
		EClass FREE_ROOM_TYPES_DTO = eINSTANCE.getFreeRoomTypesDTO();

		/**
		 * The meta object literal for the '<em><b>Room Type Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = eINSTANCE.getFreeRoomTypesDTO_RoomTypeDescription();

		/**
		 * The meta object literal for the '<em><b>Num Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_BEDS = eINSTANCE.getFreeRoomTypesDTO_NumBeds();

		/**
		 * The meta object literal for the '<em><b>Price Per Night</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = eINSTANCE.getFreeRoomTypesDTO_PricePerNight();

		/**
		 * The meta object literal for the '<em><b>Num Free Rooms</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = eINSTANCE.getFreeRoomTypesDTO_NumFreeRooms();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getIHotelStartupProvides()
		 * @generated
		 */
		EClass IHOTEL_STARTUP_PROVIDES = eINSTANCE.getIHotelStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Startup</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_STARTUP_PROVIDES___STARTUP__INT = eINSTANCE.getIHotelStartupProvides__Startup__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.HotelCustomerProvidesImpl <em>Hotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.HotelCustomerProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getHotelCustomerProvides()
		 * @generated
		 */
		EClass HOTEL_CUSTOMER_PROVIDES = eINSTANCE.getHotelCustomerProvides();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.HotelStartupProvidesImpl <em>Hotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.HotelStartupProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getHotelStartupProvides()
		 * @generated
		 */
		EClass HOTEL_STARTUP_PROVIDES = eINSTANCE.getHotelStartupProvides();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ExternalSupplierSystemImpl <em>External Supplier System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ExternalSupplierSystemImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getExternalSupplierSystem()
		 * @generated
		 */
		EClass EXTERNAL_SUPPLIER_SYSTEM = eINSTANCE.getExternalSupplierSystem();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking <em>Core Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getCoreBooking()
		 * @generated
		 */
		EClass CORE_BOOKING = eINSTANCE.getCoreBooking();

		/**
		 * The meta object literal for the '<em><b>Create Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CORE_BOOKING___CREATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getCoreBooking__CreateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Check Out Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CORE_BOOKING___CHECK_OUT_BOOKING__INT = eINSTANCE.getCoreBooking__CheckOutBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CORE_BOOKING___CHECK_IN_ROOM__STRING_INT = eINSTANCE.getCoreBooking__CheckInRoom__String_int();

		/**
		 * The meta object literal for the '<em><b>Check Out Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CORE_BOOKING___CHECK_OUT_ROOM__INT_INT = eINSTANCE.getCoreBooking__CheckOutRoom__int_int();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CORE_BOOKING___ADD_ROOM_TO_BOOKING__INT_STRING = eINSTANCE.getCoreBooking__AddRoomToBooking__int_String();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CORE_BOOKING___CONFIRM_BOOKING__INT = eINSTANCE.getCoreBooking__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Get Booking From Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CORE_BOOKING___GET_BOOKING_FROM_ROOM__INT = eINSTANCE.getCoreBooking__GetBookingFromRoom__int();

		/**
		 * The meta object literal for the '<em><b>Get Booking From Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CORE_BOOKING___GET_BOOKING_FROM_NAME__STRING_STRING = eINSTANCE.getCoreBooking__GetBookingFromName__String_String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ReceptionistImpl <em>Receptionist</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ReceptionistImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getReceptionist()
		 * @generated
		 */
		EClass RECEPTIONIST = eINSTANCE.getReceptionist();

		/**
		 * The meta object literal for the '<em><b>Change Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CHANGE_DATE__INT_STRING_STRING = eINSTANCE.getReceptionist__ChangeDate__int_String_String();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___LIST_BOOKINGS = eINSTANCE.getReceptionist__ListBookings();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___LIST_CHECK_INS__STRING_STRING = eINSTANCE.getReceptionist__ListCheckIns__String_String();

		/**
		 * The meta object literal for the '<em><b>Check In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CHECK_IN__INT = eINSTANCE.getReceptionist__CheckIn__int();

		/**
		 * The meta object literal for the '<em><b>Remove Room From Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___REMOVE_ROOM_FROM_BOOKING__INT_STRING = eINSTANCE.getReceptionist__RemoveRoomFromBooking__int_String();

		/**
		 * The meta object literal for the '<em><b>Get Booking Room Reservations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___GET_BOOKING_ROOM_RESERVATIONS__BOOKING = eINSTANCE.getReceptionist__GetBookingRoomReservations__Booking();

		/**
		 * The meta object literal for the '<em><b>List Check Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___LIST_CHECK_OUT__STRING_STRING = eINSTANCE.getReceptionist__ListCheckOut__String_String();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CANCEL_BOOKING__INT = eINSTANCE.getReceptionist__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Get Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___GET_BOOKING__INT = eINSTANCE.getReceptionist__GetBooking__int();

		/**
		 * The meta object literal for the '<em><b>Change Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___CHANGE_ROOM_TYPE__INT_STRING_STRING = eINSTANCE.getReceptionist__ChangeRoomType__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE = eINSTANCE.getReceptionist__AddExtraCost__int_int_String_double();

		/**
		 * The meta object literal for the '<em><b>List Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEPTIONIST___LIST_OCCUPIED_ROOMS__STRING = eINSTANCE.getReceptionist__ListOccupiedRooms__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking <em>Manage Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getManageBooking()
		 * @generated
		 */
		EClass MANAGE_BOOKING = eINSTANCE.getManageBooking();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_BOOKING___LIST_BOOKINGS = eINSTANCE.getManageBooking__ListBookings();

		/**
		 * The meta object literal for the '<em><b>Change Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_BOOKING___CHANGE_ROOM_TYPE__INT_STRING_STRING = eINSTANCE.getManageBooking__ChangeRoomType__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room From Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_BOOKING___REMOVE_ROOM_FROM_BOOKING__INT_STRING = eINSTANCE.getManageBooking__RemoveRoomFromBooking__int_String();

		/**
		 * The meta object literal for the '<em><b>Change Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_BOOKING___CHANGE_DATE__INT_STRING_STRING = eINSTANCE.getManageBooking__ChangeDate__int_String_String();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_BOOKING___LIST_CHECK_INS__STRING_STRING = eINSTANCE.getManageBooking__ListCheckIns__String_String();

		/**
		 * The meta object literal for the '<em><b>List Check Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_BOOKING___LIST_CHECK_OUT__STRING_STRING = eINSTANCE.getManageBooking__ListCheckOut__String_String();

		/**
		 * The meta object literal for the '<em><b>Check In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_BOOKING___CHECK_IN__INT = eINSTANCE.getManageBooking__CheckIn__int();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_BOOKING___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE = eINSTANCE.getManageBooking__AddExtraCost__int_int_String_double();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_BOOKING___CANCEL_BOOKING__INT = eINSTANCE.getManageBooking__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Get Booking Room Reservations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_BOOKING___GET_BOOKING_ROOM_RESERVATIONS__BOOKING = eINSTANCE.getManageBooking__GetBookingRoomReservations__Booking();

		/**
		 * The meta object literal for the '<em><b>Get Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_BOOKING___GET_BOOKING__INT = eINSTANCE.getManageBooking__GetBooking__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl <em>Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getBooking()
		 * @generated
		 */
		EClass BOOKING = eINSTANCE.getBooking();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__ID = eINSTANCE.getBooking_Id();

		/**
		 * The meta object literal for the '<em><b>Start Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__START_DATE = eINSTANCE.getBooking_StartDate();

		/**
		 * The meta object literal for the '<em><b>End Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__END_DATE = eINSTANCE.getBooking_EndDate();

		/**
		 * The meta object literal for the '<em><b>Amount Paid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__AMOUNT_PAID = eINSTANCE.getBooking_AmountPaid();

		/**
		 * The meta object literal for the '<em><b>Guest Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__GUEST_NAME = eINSTANCE.getBooking_GuestName();

		/**
		 * The meta object literal for the '<em><b>Reservations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__RESERVATIONS = eINSTANCE.getBooking_Reservations();

		/**
		 * The meta object literal for the '<em><b>Confirmed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__CONFIRMED = eINSTANCE.getBooking_Confirmed();

		/**
		 * The meta object literal for the '<em><b>Is Payed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__IS_PAYED = eINSTANCE.getBooking_IsPayed();

		/**
		 * The meta object literal for the '<em><b>Checked In</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__CHECKED_IN = eINSTANCE.getBooking_CheckedIn();

		/**
		 * The meta object literal for the '<em><b>Checked Out</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__CHECKED_OUT = eINSTANCE.getBooking_CheckedOut();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___ADD_ROOM__ROOM = eINSTANCE.getBooking__AddRoom__Room();

		/**
		 * The meta object literal for the '<em><b>Get Total Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___GET_TOTAL_COST = eINSTANCE.getBooking__GetTotalCost();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___REMOVE_ROOM__ROOMTYPE = eINSTANCE.getBooking__RemoveRoom__RoomType();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomReservationImpl <em>Room Reservation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomReservationImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getRoomReservation()
		 * @generated
		 */
		EClass ROOM_RESERVATION = eINSTANCE.getRoomReservation();

		/**
		 * The meta object literal for the '<em><b>Room Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_RESERVATION__ROOM_ID = eINSTANCE.getRoomReservation_RoomId();

		/**
		 * The meta object literal for the '<em><b>Booking</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_RESERVATION__BOOKING = eINSTANCE.getRoomReservation_Booking();

		/**
		 * The meta object literal for the '<em><b>Room Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_RESERVATION__ROOM_TYPE = eINSTANCE.getRoomReservation_RoomType();

		/**
		 * The meta object literal for the '<em><b>Extra Cost</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_RESERVATION__EXTRA_COST = eINSTANCE.getRoomReservation_ExtraCost();

		/**
		 * The meta object literal for the '<em><b>Is Checked Out</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_RESERVATION__IS_CHECKED_OUT = eINSTANCE.getRoomReservation_IsCheckedOut();

		/**
		 * The meta object literal for the '<em><b>Get Total Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_RESERVATION___GET_TOTAL_COST = eINSTANCE.getRoomReservation__GetTotalCost();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_RESERVATION___ADD_EXTRA_COST__STRING_DOUBLE = eINSTANCE.getRoomReservation__AddExtraCost__String_double();

		/**
		 * The meta object literal for the '<em><b>Check Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_RESERVATION___CHECK_OUT = eINSTANCE.getRoomReservation__CheckOut();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingManagerImpl <em>Booking Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getBookingManager()
		 * @generated
		 */
		EClass BOOKING_MANAGER = eINSTANCE.getBookingManager();

		/**
		 * The meta object literal for the '<em><b>Bookings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_MANAGER__BOOKINGS = eINSTANCE.getBookingManager_Bookings();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClearBooking <em>Clear Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClearBooking
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getClearBooking()
		 * @generated
		 */
		EClass CLEAR_BOOKING = eINSTANCE.getClearBooking();

		/**
		 * The meta object literal for the '<em><b>Clear Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CLEAR_BOOKING___CLEAR_BOOKINGS = eINSTANCE.getClearBooking__ClearBookings();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Room Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__ROOM_NUMBER = eINSTANCE.getRoom_RoomNumber();

		/**
		 * The meta object literal for the '<em><b>Is Free</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__IS_FREE = eINSTANCE.getRoom_IsFree();

		/**
		 * The meta object literal for the '<em><b>Is Blocked</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__IS_BLOCKED = eINSTANCE.getRoom_IsBlocked();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__TYPE = eINSTANCE.getRoom_Type();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomTypeImpl <em>Room Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomTypeImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getRoomType()
		 * @generated
		 */
		EClass ROOM_TYPE = eINSTANCE.getRoomType();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__TYPE = eINSTANCE.getRoomType_Type();

		/**
		 * The meta object literal for the '<em><b>Number Of Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NUMBER_OF_BEDS = eINSTANCE.getRoomType_NumberOfBeds();

		/**
		 * The meta object literal for the '<em><b>Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__COST = eINSTANCE.getRoomType_Cost();

		/**
		 * The meta object literal for the '<em><b>Additional Features</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__ADDITIONAL_FEATURES = eINSTANCE.getRoomType_AdditionalFeatures();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ExtraCostImpl <em>Extra Cost</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ExtraCostImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getExtraCost()
		 * @generated
		 */
		EClass EXTRA_COST = eINSTANCE.getExtraCost();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA_COST__DESCRIPTION = eINSTANCE.getExtraCost_Description();

		/**
		 * The meta object literal for the '<em><b>Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA_COST__COST = eINSTANCE.getExtraCost_Cost();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomManagerImpl <em>Room Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getRoomManager()
		 * @generated
		 */
		EClass ROOM_MANAGER = eINSTANCE.getRoomManager();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_MANAGER__ROOMS = eINSTANCE.getRoomManager_Rooms();

		/**
		 * The meta object literal for the '<em><b>Room Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_MANAGER__ROOM_TYPES = eINSTANCE.getRoomManager_RoomTypes();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms <em>List Rooms</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getListRooms()
		 * @generated
		 */
		EClass LIST_ROOMS = eINSTANCE.getListRooms();

		/**
		 * The meta object literal for the '<em><b>List Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LIST_ROOMS___LIST_ROOMS = eINSTANCE.getListRooms__ListRooms();

		/**
		 * The meta object literal for the '<em><b>List Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LIST_ROOMS___LIST_OCCUPIED_ROOMS__STRING = eINSTANCE.getListRooms__ListOccupiedRooms__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListFreeRooms <em>List Free Rooms</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListFreeRooms
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getListFreeRooms()
		 * @generated
		 */
		EClass LIST_FREE_ROOMS = eINSTANCE.getListFreeRooms();

		/**
		 * The meta object literal for the '<em><b>List Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LIST_FREE_ROOMS___LIST_FREE_ROOMS__STRING_STRING_INT = eINSTANCE.getListFreeRooms__ListFreeRooms__String_String_int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager <em>Manage Room Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getManageRoomManager()
		 * @generated
		 */
		EClass MANAGE_ROOM_MANAGER = eINSTANCE.getManageRoomManager();

		/**
		 * The meta object literal for the '<em><b>Change Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_ROOM_MANAGER___CHANGE_ROOM_TYPE__INT_ROOMTYPE = eINSTANCE.getManageRoomManager__ChangeRoomType__int_RoomType();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_ROOM_MANAGER___REMOVE_ROOM__INT = eINSTANCE.getManageRoomManager__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_ROOM_MANAGER___BLOCK_ROOM__INT = eINSTANCE.getManageRoomManager__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_ROOM_MANAGER___UNBLOCK_ROOM__INT = eINSTANCE.getManageRoomManager__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_ROOM_MANAGER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getManageRoomManager__UpdateRoomType__String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_ROOM_MANAGER___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getManageRoomManager__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Get Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MANAGE_ROOM_MANAGER___GET_ROOM_TYPE__STRING = eINSTANCE.getManageRoomManager__GetRoomType__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager <em>Add Clear Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getAddClearManager()
		 * @generated
		 */
		EClass ADD_CLEAR_MANAGER = eINSTANCE.getAddClearManager();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADD_CLEAR_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getAddClearManager__AddRoomType__String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADD_CLEAR_MANAGER___ADD_ROOM__INT_ROOMTYPE = eINSTANCE.getAddClearManager__AddRoom__int_RoomType();

		/**
		 * The meta object literal for the '<em><b>Clear Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADD_CLEAR_MANAGER___CLEAR_ROOMS = eINSTANCE.getAddClearManager__ClearRooms();

		/**
		 * The meta object literal for the '<em><b>Clear Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADD_CLEAR_MANAGER___CLEAR_ROOM_TYPES = eINSTANCE.getAddClearManager__ClearRoomTypes();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.AdminImpl <em>Admin</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.AdminImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl#getAdmin()
		 * @generated
		 */
		EClass ADMIN = eINSTANCE.getAdmin();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___UNBLOCK_ROOM__INT = eINSTANCE.getAdmin__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getAdmin__UpdateRoomType__String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Get Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___GET_ROOM_TYPE__STRING = eINSTANCE.getAdmin__GetRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___REMOVE_ROOM__INT = eINSTANCE.getAdmin__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Change Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___CHANGE_ROOM_TYPE__INT_ROOMTYPE = eINSTANCE.getAdmin__ChangeRoomType__int_RoomType();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getAdmin__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___BLOCK_ROOM__INT = eINSTANCE.getAdmin__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Startup</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___STARTUP__INT = eINSTANCE.getAdmin__Startup__int();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___ADD_ROOM__INT_ROOMTYPE = eINSTANCE.getAdmin__AddRoom__int_RoomType();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ADMIN___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getAdmin__AddRoomType__String_double_int_String();

	}

} //ClassDiagramComponentsPackage
