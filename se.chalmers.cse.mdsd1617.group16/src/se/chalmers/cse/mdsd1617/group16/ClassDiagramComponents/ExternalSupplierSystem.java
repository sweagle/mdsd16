/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Supplier System</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage#getExternalSupplierSystem()
 * @model
 * @generated
 */
public interface ExternalSupplierSystem extends HotelCustomerProvides {
} // ExternalSupplierSystem
