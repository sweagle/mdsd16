/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage#getHotelCustomerProvides()
 * @model
 * @generated
 */
public interface HotelCustomerProvides extends IHotelCustomerProvides {
} // HotelCustomerProvides
