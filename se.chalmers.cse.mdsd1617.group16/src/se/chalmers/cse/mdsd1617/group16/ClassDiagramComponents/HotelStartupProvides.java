/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage#getHotelStartupProvides()
 * @model
 * @generated
 */
public interface HotelStartupProvides extends IHotelStartupProvides {
} // HotelStartupProvides
