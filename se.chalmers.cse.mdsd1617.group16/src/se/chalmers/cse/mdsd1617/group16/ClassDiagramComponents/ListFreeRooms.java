/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List Free Rooms</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage#getListFreeRooms()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ListFreeRooms extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false" minBedsRequired="true" minBedsOrdered="false"
	 * @generated
	 */
	EList<FreeRoomTypesDTO> listFreeRooms(String startDate, String endDate, int minBeds);

} // ListFreeRooms
