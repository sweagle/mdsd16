/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents;

import java.util.Map;
import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List Rooms</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage#getListRooms()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ListRooms extends ListFreeRooms {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false"
	 * @generated
	 */
	EList<Room> listRooms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	Map<Integer, Room> listOccupiedRooms(String date);

} // ListRooms
