/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager#getRooms <em>Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager#getRoomTypes <em>Room Types</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage#getRoomManager()
 * @model
 * @generated
 */
public interface RoomManager extends ManageRoomManager {
	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' containment reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' containment reference list.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage#getRoomManager_Rooms()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Room> getRooms();

	/**
	 * Returns the value of the '<em><b>Room Types</b></em>' containment reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Types</em>' containment reference list.
	 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage#getRoomManager_RoomTypes()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<RoomType> getRoomTypes();

} // RoomManager
