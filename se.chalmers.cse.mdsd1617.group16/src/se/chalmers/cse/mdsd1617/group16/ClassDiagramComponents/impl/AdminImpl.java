/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Admin</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AdminImpl extends MinimalEObjectImpl.Container implements Admin {
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AdminImpl() {
		super();
	}



	private ManageRoomManager rManager;
	private IHotelStartupProvides hStartup;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public AdminImpl(ManageRoomManager rManager, IHotelStartupProvides hStartup) {
		super();
		this.setrManager(rManager);
		this.sethStartup(hStartup);
	}
	
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassDiagramComponentsPackage.Literals.ADMIN;
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void unblockRoom(int roomNumber) {
		rManager.unblockRoom(roomNumber);
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void updateRoomType(String name, double price, int numberOfBeds, String additionalFeatures) {
		rManager.updateRoomType(name, price, numberOfBeds, additionalFeatures);
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public RoomType getRoomType(String roomTypeDescription) {
		return rManager.getRoomType(roomTypeDescription);
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void removeRoom(int roomNumber) {
		rManager.removeRoom(roomNumber);
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void changeRoomType(int roomNumber, RoomType roomType) {
		rManager.changeRoomType(roomNumber, roomType);
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean removeRoomType(String name) {
		return rManager.removeRoomType(name);
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void blockRoom(int roomNumber) {
		rManager.blockRoom(roomNumber);
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void startup(int numRooms) {
		
		this.hStartup.startup(20);
		
		
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void addRoom(int roomNumber, RoomType roomType) {
		
		rManager.addRoom(roomNumber, roomType);
		
		
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void addRoomType(String name, double price, int numBeds, String addFeatures) {
		rManager.addRoomType(name, price, numBeds, addFeatures);
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ClassDiagramComponentsPackage.ADMIN___UNBLOCK_ROOM__INT:
				unblockRoom((Integer)arguments.get(0));
				return null;
			case ClassDiagramComponentsPackage.ADMIN___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				updateRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
				return null;
			case ClassDiagramComponentsPackage.ADMIN___GET_ROOM_TYPE__STRING:
				return getRoomType((String)arguments.get(0));
			case ClassDiagramComponentsPackage.ADMIN___REMOVE_ROOM__INT:
				removeRoom((Integer)arguments.get(0));
				return null;
			case ClassDiagramComponentsPackage.ADMIN___CHANGE_ROOM_TYPE__INT_ROOMTYPE:
				changeRoomType((Integer)arguments.get(0), (RoomType)arguments.get(1));
				return null;
			case ClassDiagramComponentsPackage.ADMIN___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case ClassDiagramComponentsPackage.ADMIN___BLOCK_ROOM__INT:
				blockRoom((Integer)arguments.get(0));
				return null;
			case ClassDiagramComponentsPackage.ADMIN___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
			case ClassDiagramComponentsPackage.ADMIN___ADD_ROOM__INT_ROOMTYPE:
				addRoom((Integer)arguments.get(0), (RoomType)arguments.get(1));
				return null;
			case ClassDiagramComponentsPackage.ADMIN___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}



	public ManageRoomManager getrManager() {
		return rManager;
	}



	public void setrManager(ManageRoomManager rManager) {
		this.rManager = rManager;
	}



	public IHotelStartupProvides gethStartup() {
		return hStartup;
	}



	public void sethStartup(IHotelStartupProvides hStartup) {
		this.hStartup = hStartup;
	}

} //AdminImpl
