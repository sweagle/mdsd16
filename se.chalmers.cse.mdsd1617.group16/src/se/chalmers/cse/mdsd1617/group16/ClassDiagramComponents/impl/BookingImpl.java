/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl#getId <em>Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl#getStartDate <em>Start Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl#getEndDate <em>End Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl#getAmountPaid <em>Amount Paid</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl#getGuestName <em>Guest Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl#getReservations <em>Reservations</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl#isConfirmed <em>Confirmed</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl#isPayed <em>Is Payed</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl#isCheckedIn <em>Checked In</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingImpl#isCheckedOut <em>Checked Out</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingImpl extends MinimalEObjectImpl.Container implements Booking {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartDate() <em>Start Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartDate()
	 * @generated
	 * @ordered
	 */
	protected static final String START_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartDate() <em>Start Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartDate()
	 * @generated
	 * @ordered
	 */
	protected String startDate = START_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndDate() <em>End Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndDate()
	 * @generated
	 * @ordered
	 */
	protected static final String END_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndDate() <em>End Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndDate()
	 * @generated
	 * @ordered
	 */
	protected String endDate = END_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAmountPaid() <em>Amount Paid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAmountPaid()
	 * @generated
	 * @ordered
	 */
	protected static final double AMOUNT_PAID_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getAmountPaid() <em>Amount Paid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAmountPaid()
	 * @generated
	 * @ordered
	 */
	protected double amountPaid = AMOUNT_PAID_EDEFAULT;

	/**
	 * The default value of the '{@link #getGuestName() <em>Guest Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuestName()
	 * @generated
	 * @ordered
	 */
	protected static final String GUEST_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGuestName() <em>Guest Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuestName()
	 * @generated
	 * @ordered
	 */
	protected String guestName = GUEST_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReservations() <em>Reservations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservations()
	 * @generated
	 * @ordered
	 */
	protected EList<RoomReservation> reservations;

	/**
	 * The default value of the '{@link #isConfirmed() <em>Confirmed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConfirmed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONFIRMED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isConfirmed() <em>Confirmed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConfirmed()
	 * @generated
	 * @ordered
	 */
	protected boolean confirmed = CONFIRMED_EDEFAULT;

	/**
	 * The default value of the '{@link #isPayed() <em>Is Payed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPayed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_PAYED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPayed() <em>Is Payed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPayed()
	 * @generated
	 * @ordered
	 */
	protected boolean isPayed = IS_PAYED_EDEFAULT;

	/**
	 * The default value of the '{@link #isCheckedIn() <em>Checked In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCheckedIn()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CHECKED_IN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCheckedIn() <em>Checked In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCheckedIn()
	 * @generated
	 * @ordered
	 */
	protected boolean checkedIn = CHECKED_IN_EDEFAULT;

	/**
	 * The default value of the '{@link #isCheckedOut() <em>Checked Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCheckedOut()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CHECKED_OUT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCheckedOut() <em>Checked Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCheckedOut()
	 * @generated
	 * @ordered
	 */
	protected boolean checkedOut = CHECKED_OUT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	protected BookingImpl() {
		super();
		reservations = new BasicEList<RoomReservation>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassDiagramComponentsPackage.Literals.BOOKING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setId(int newId) {

		id = newId;		
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public String getStartDate() {
		
		return startDate;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setStartDate(String newStartDate) {
		
		startDate = newStartDate;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setEndDate(String newEndDate) {
		
		endDate = newEndDate;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public double getAmountPaid() {
		
		return amountPaid;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setAmountPaid(double newAmountPaid) {	
		
		amountPaid = newAmountPaid;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public String getGuestName() {
		
		return guestName;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setGuestName(String newGuestName) {
		
		guestName = newGuestName;	
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoomReservation> getReservations() {
		if (reservations == null) {
			reservations = new EObjectContainmentEList<RoomReservation>(RoomReservation.class, this, ClassDiagramComponentsPackage.BOOKING__RESERVATIONS);
		}
		return reservations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean isConfirmed() {
		
		return confirmed;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setConfirmed(boolean newConfirmed) {	
		
		confirmed = newConfirmed;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean isPayed() {
		
		return isPayed;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setIsPayed(boolean newIsPayed) {
		
		isPayed = newIsPayed;	
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCheckedIn() {
		return checkedIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setCheckedIn(boolean newCheckedIn) {
		
		checkedIn = newCheckedIn;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean isCheckedOut() {
		
		return checkedOut;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setCheckedOut(boolean newCheckedOut) {
		
		checkedOut = newCheckedOut;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public void addRoom(Room room) {
		RoomReservation rr = new RoomReservationImpl();
		rr.setBooking(this);
		rr.setRoomType(room.getType());
		rr.setRoomId(room.getRoomNumber());
		reservations.add(rr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public double getTotalCost() {
		double sum = 0;
		for(RoomReservation rr: reservations){
			sum+=rr.getTotalCost();			
		}
		return sum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean removeRoom(RoomType roomType) {
		for(RoomReservation rr : reservations){
			if(rr.getRoomType().equals(roomType)){
				return reservations.remove(rr);
			}
			
		}
		return true;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.BOOKING__RESERVATIONS:
				return ((InternalEList<?>)getReservations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.BOOKING__ID:
				return getId();
			case ClassDiagramComponentsPackage.BOOKING__START_DATE:
				return getStartDate();
			case ClassDiagramComponentsPackage.BOOKING__END_DATE:
				return getEndDate();
			case ClassDiagramComponentsPackage.BOOKING__AMOUNT_PAID:
				return getAmountPaid();
			case ClassDiagramComponentsPackage.BOOKING__GUEST_NAME:
				return getGuestName();
			case ClassDiagramComponentsPackage.BOOKING__RESERVATIONS:
				return getReservations();
			case ClassDiagramComponentsPackage.BOOKING__CONFIRMED:
				return isConfirmed();
			case ClassDiagramComponentsPackage.BOOKING__IS_PAYED:
				return isPayed();
			case ClassDiagramComponentsPackage.BOOKING__CHECKED_IN:
				return isCheckedIn();
			case ClassDiagramComponentsPackage.BOOKING__CHECKED_OUT:
				return isCheckedOut();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.BOOKING__ID:
				setId((Integer)newValue);
				return;
			case ClassDiagramComponentsPackage.BOOKING__START_DATE:
				setStartDate((String)newValue);
				return;
			case ClassDiagramComponentsPackage.BOOKING__END_DATE:
				setEndDate((String)newValue);
				return;
			case ClassDiagramComponentsPackage.BOOKING__AMOUNT_PAID:
				setAmountPaid((Double)newValue);
				return;
			case ClassDiagramComponentsPackage.BOOKING__GUEST_NAME:
				setGuestName((String)newValue);
				return;
			case ClassDiagramComponentsPackage.BOOKING__RESERVATIONS:
				getReservations().clear();
				getReservations().addAll((Collection<? extends RoomReservation>)newValue);
				return;
			case ClassDiagramComponentsPackage.BOOKING__CONFIRMED:
				setConfirmed((Boolean)newValue);
				return;
			case ClassDiagramComponentsPackage.BOOKING__IS_PAYED:
				setIsPayed((Boolean)newValue);
				return;
			case ClassDiagramComponentsPackage.BOOKING__CHECKED_IN:
				setCheckedIn((Boolean)newValue);
				return;
			case ClassDiagramComponentsPackage.BOOKING__CHECKED_OUT:
				setCheckedOut((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.BOOKING__ID:
				setId(ID_EDEFAULT);
				return;
			case ClassDiagramComponentsPackage.BOOKING__START_DATE:
				setStartDate(START_DATE_EDEFAULT);
				return;
			case ClassDiagramComponentsPackage.BOOKING__END_DATE:
				setEndDate(END_DATE_EDEFAULT);
				return;
			case ClassDiagramComponentsPackage.BOOKING__AMOUNT_PAID:
				setAmountPaid(AMOUNT_PAID_EDEFAULT);
				return;
			case ClassDiagramComponentsPackage.BOOKING__GUEST_NAME:
				setGuestName(GUEST_NAME_EDEFAULT);
				return;
			case ClassDiagramComponentsPackage.BOOKING__RESERVATIONS:
				getReservations().clear();
				return;
			case ClassDiagramComponentsPackage.BOOKING__CONFIRMED:
				setConfirmed(CONFIRMED_EDEFAULT);
				return;
			case ClassDiagramComponentsPackage.BOOKING__IS_PAYED:
				setIsPayed(IS_PAYED_EDEFAULT);
				return;
			case ClassDiagramComponentsPackage.BOOKING__CHECKED_IN:
				setCheckedIn(CHECKED_IN_EDEFAULT);
				return;
			case ClassDiagramComponentsPackage.BOOKING__CHECKED_OUT:
				setCheckedOut(CHECKED_OUT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.BOOKING__ID:
				return id != ID_EDEFAULT;
			case ClassDiagramComponentsPackage.BOOKING__START_DATE:
				return START_DATE_EDEFAULT == null ? startDate != null : !START_DATE_EDEFAULT.equals(startDate);
			case ClassDiagramComponentsPackage.BOOKING__END_DATE:
				return END_DATE_EDEFAULT == null ? endDate != null : !END_DATE_EDEFAULT.equals(endDate);
			case ClassDiagramComponentsPackage.BOOKING__AMOUNT_PAID:
				return amountPaid != AMOUNT_PAID_EDEFAULT;
			case ClassDiagramComponentsPackage.BOOKING__GUEST_NAME:
				return GUEST_NAME_EDEFAULT == null ? guestName != null : !GUEST_NAME_EDEFAULT.equals(guestName);
			case ClassDiagramComponentsPackage.BOOKING__RESERVATIONS:
				return reservations != null && !reservations.isEmpty();
			case ClassDiagramComponentsPackage.BOOKING__CONFIRMED:
				return confirmed != CONFIRMED_EDEFAULT;
			case ClassDiagramComponentsPackage.BOOKING__IS_PAYED:
				return isPayed != IS_PAYED_EDEFAULT;
			case ClassDiagramComponentsPackage.BOOKING__CHECKED_IN:
				return checkedIn != CHECKED_IN_EDEFAULT;
			case ClassDiagramComponentsPackage.BOOKING__CHECKED_OUT:
				return checkedOut != CHECKED_OUT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ClassDiagramComponentsPackage.BOOKING___ADD_ROOM__ROOM:
				addRoom((Room)arguments.get(0));
				return null;
			case ClassDiagramComponentsPackage.BOOKING___GET_TOTAL_COST:
				return getTotalCost();
			case ClassDiagramComponentsPackage.BOOKING___REMOVE_ROOM__ROOMTYPE:
				return removeRoom((RoomType)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", startDate: ");
		result.append(startDate);
		result.append(", endDate: ");
		result.append(endDate);
		result.append(", amountPaid: ");
		result.append(amountPaid);
		result.append(", guestName: ");
		result.append(guestName);
		result.append(", confirmed: ");
		result.append(confirmed);
		result.append(", isPayed: ");
		result.append(isPayed);
		result.append(", checkedIn: ");
		result.append(checkedIn);
		result.append(", checkedOut: ");
		result.append(checkedOut);
		result.append(')');
		return result.toString();
	}

} //BookingImpl
