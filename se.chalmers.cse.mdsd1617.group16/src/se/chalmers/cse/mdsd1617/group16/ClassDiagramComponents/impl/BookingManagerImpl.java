/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.BookingManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListFreeRooms;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingManagerImpl#getBookings <em>Bookings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingManagerImpl extends MinimalEObjectImpl.Container implements BookingManager {
	/**
	 * The cached value of the '{@link #getBookings() <em>Bookings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookings()
	 * @generated
	 * @ordered
	 */
	protected EList<Booking> bookings;
	private static BookingManager theBookingManagerImpl;

	private int bookingIdCounter;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated Not
	 */
	public BookingManagerImpl() {
		super();
		bookingIdCounter = 0;
		roomManager = RoomManagerImpl.getRoomManagerImpl(); //TODO singleton room manager?
		bookings = new BasicEList<Booking>();
	}

	public static BookingManagerImpl getBookingManagerImpl() {
		if(theBookingManagerImpl == null){
			theBookingManagerImpl = new BookingManagerImpl();
		}
		return (BookingManagerImpl) theBookingManagerImpl;
	}

	protected RoomManager roomManager;
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassDiagramComponentsPackage.Literals.BOOKING_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Booking> getBookings() {
		if (bookings == null) {
			bookings = new EObjectContainmentEList<Booking>(Booking.class, this, ClassDiagramComponentsPackage.BOOKING_MANAGER__BOOKINGS);
		}
		return bookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public void clearBookings() {
		bookings.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public int createBooking(String firstName, String lastName, String startDate, String endDate) {
		if (firstName == null || lastName == null || firstName.equals("") ||lastName.equals("") ||
				startDate == null || endDate == null || !validDate(startDate,endDate)){
			return -1;
		}
		Booking newBooking = new BookingImpl();
		newBooking.setGuestName(firstName + " " + lastName);
		newBooking.setStartDate(startDate);
		newBooking.setEndDate(endDate);
		newBooking.setId(bookingIdCounter);

		bookingIdCounter++;

		bookings.add(newBooking);

		return newBooking.getId();
	}

	//If not parseble, return false. If Date is ok, return true
	public boolean validDate(String startDate, String endDate) {
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		df.setLenient(false);

		Date today = new Date();
		Date start = null;
		Date end = null;
		try {
			start = df.parse(startDate);
			end = df.parse(endDate);
		} catch (Exception e) {
			return false;
		}

		if (end.before(start) ||start.equals(end) ||end.before(today)) return false;

		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public double checkOutBooking(int bookingId) {
		Booking b = getBooking(bookingId);
		if (b == null) {
			return -1;
		}
		for(RoomReservation rr : b.getReservations()){
			rr.checkOut();
		}
		b.setCheckedOut(true);
		return b.getTotalCost();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public int checkInRoom(String roomType, int bookingId) {
		Booking b = getBooking(bookingId);
		RoomType rt = roomManager.getRoomType(roomType);
		if(b==null)
			return -1; // No valid booking
		for (RoomReservation rm : b.getReservations()) {
			if (rm.getRoomId() == -1) {
				for (Room r : roomManager.getRooms()) {
					if (r.getType().equals(rt) && r.isFree()) {
						r.setIsFree(false);
						rm.setRoomId(r.getRoomNumber());
						return r.getRoomNumber();
					}
				}
			}
		}
		return -1; 
	}




	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated nope
	 */
	public double checkOutRoom(int roomId, int bookingId) {

		Booking b = getBooking(bookingId);
		
		if(b==null)
			return -1; //Invalid booking id
			
		for(RoomReservation rr: b.getReservations()){
			if(rr.getRoomId() == roomId ){

				for(Room r: roomManager.getRooms()){

					if(r.getRoomNumber() == roomId){
						r.setIsFree(true);
						rr.checkOut();

						return rr.getTotalCost();
					}

				}

			}
		}

		return -1;
	}

	public Booking getBooking(int bookingId){

		for(Booking b: bookings){

			if (b.getId() == bookingId) {

				return b;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated Not
	 */
	public boolean addRoomToBooking(int bookingId, String roomType) {
		
		Booking b = getBooking(bookingId);
		
		if(b == null){
			return false;
		}
		
		RoomType rt = roomManager.getRoomType(roomType);
		
		if (this.roomTypesAvailable(rt, b.getStartDate(), b.getEndDate()) !=0){
			
			RoomReservation rr = new RoomReservationImpl();
			rr.setRoomType(rt);
			rr.setBooking(b);
			b.getReservations().add(rr);
			
			return true;
			
		}

		return false;
	}
	
	private int roomTypesAvailable(RoomType rt, String startDate, String endDate) {

		int sum = 0;
		for(Room r : roomManager.getRooms()){
			if(r.getType().equals(rt)){
				sum++;
			}
		}
		for(Booking b : getBookings()){
			//if(b.isConfirmed()){
				for(RoomReservation rr : b.getReservations()){
					if(rr.getRoomType().equals(rt) && bookingOverlaps(b, startDate, endDate)){
						sum--;
					}
				}
			//}
		}

		return sum;
	}

	private boolean bookingOverlaps(Booking b, String startDate, String endDate) {
		Date bStartDate = null;
		Date bEndDate = null;
		Date StartDate = null;
		Date EndDate = null;
		DateFormat df = new SimpleDateFormat("yyyymmdd");
		try {
			bStartDate = df.parse(b.getStartDate());
			bEndDate = df.parse(b.getEndDate());
			StartDate = df.parse(b.getStartDate());
			EndDate = df.parse(b.getEndDate());
		} catch (ParseException e){
			return false; // Maybe throw error?
		}
		if((bStartDate.after(StartDate) && bStartDate.before(EndDate))  // Begins during date inteval
				|| (bEndDate.after(StartDate) && bEndDate.before(EndDate)) //ends during date interbal
				|| (bStartDate.before(StartDate) && bEndDate.after(EndDate)) // Booking overlaps whole date interval
				|| (bStartDate.equals(StartDate)) || (bEndDate.equals(EndDate))){ // Starts/Ends on same day


			return true;
		}

		return false;
	}

	private Room roomTypeAvailable(RoomType rt, String startDate, String endDate) {
		for(Room r : roomManager.getRooms()){
			if(r.getType().equals(rt)){
				if(roomAvailable(r, startDate, endDate)){
					return r;
				}
			}
		}

		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean confirmBooking(int bookingID) {
		Booking booking = getBooking(bookingID);
		if (booking == null || booking.getReservations().size() == 0) {
			return false;
		}
		booking.setConfirmed(true);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public Booking getBookingFromRoom(int roomNumber) {
		
		for (Booking booking :bookings){
			for(RoomReservation reservation : booking.getReservations()){
				if(reservation.getRoomId()==roomNumber){
					return booking;
				}
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public Booking getBookingFromName(String firstName, String lastName) {
		for(Booking b : getBookings()){
			if(b.getGuestName().equals(firstName + " " + lastName)){
				return b;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated Nop
	 */
	public EList<FreeRoomTypesDTO> listFreeRooms(String startDate, String endDate, int minBeds) {
		EList<FreeRoomTypesDTO> freeRooms = new BasicEList<FreeRoomTypesDTO>();
		//boolean alreadyInList = false;

		if(!validDate(startDate, endDate)) return freeRooms;

		for(RoomType rt: roomManager.getRoomTypes()){


			if(rt.getNumberOfBeds() >= minBeds){
				int numberOfFreeRooms = numberOfRoomsAvailable(rt.getType(), startDate, endDate);
				if(numberOfFreeRooms != 0){
					FreeRoomTypesDTO free = new FreeRoomTypesDTOImpl();
					free.setNumBeds(rt.getNumberOfBeds());
					free.setPricePerNight(rt.getCost());
					free.setRoomTypeDescription(rt.getType());
					free.setNumFreeRooms(numberOfFreeRooms);
					freeRooms.add(free);
				}

			}

		}

		return freeRooms;
	}

	private int numberOfRoomsAvailable(String roomType, String startDate, String endDate){
		int numberOfRoomsNotAvailable = 0;
		DateFormat df = new SimpleDateFormat("yyyymmdd");
		Date resStartDate = null;
		Date resEndDate = null;
		Date start = null;
		Date end = null;
		try {
			start = df.parse(startDate);
			end = df.parse(endDate);
		} catch (ParseException e) {
			return 0;
		}

		for(Booking b: bookings){
			if(b.isConfirmed()){

				for(RoomReservation reservation : b.getReservations()){
					if (roomType.equals( reservation.getRoomType().getType())){
						try {
							resStartDate = df.parse(reservation.getBooking().getStartDate());
							resEndDate = df.parse(reservation.getBooking().getEndDate());
						} catch (ParseException e){
							return 0; // Maybe throw error?
						}
						if((start.after(resStartDate) && start.before(resEndDate))
								|| (end.after(resStartDate) && end.before(resEndDate))
								|| (start.before(resStartDate) && end.after(resEndDate))
								|| (start.equals(resStartDate))){

							numberOfRoomsNotAvailable++;
						}
					}
				}
			}
		}

		int totalNumberOfRooms = 0;

		for(Room r: roomManager.getRooms()){
			if(r.getType().getType().equals(roomType)) {
				totalNumberOfRooms++;
			}
		}

		return totalNumberOfRooms - numberOfRoomsNotAvailable;

	}


	private boolean roomAvailable(Room room, String startDate, String endDate) {
		// YYYYMMDD
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date resStartDate = null;
		Date resEndDate = null;
		Date start = null;
		Date end = null;
		try {
			start = df.parse(startDate);
			end = df.parse(endDate);
		} catch (ParseException e) {
			return false;
		}

		for(Booking b: bookings){

			if(b.isConfirmed()){

				for(RoomReservation reservation : b.getReservations()){
					if (room.getRoomNumber() == reservation.getRoomId()){
						try {
							resEndDate = df.parse(reservation.getBooking().getEndDate());
						} catch (ParseException e){
							return false;
						}

						if(!(start.before(end) && (end.before(resStartDate)) || start.after(resEndDate))){
							return false;
						}

					}
				}
			}
		}
		return true;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated Nonono
	 */
	public EList<Room> listRooms() {
		return roomManager.getRooms();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Map<Integer, Room> listOccupiedRooms(String date) {
		Map<Integer, Room> occupiedRooms = new HashMap<Integer, Room>();
		/*
		 * Identifies occupied rooms and puts the room along with the 
		 * corresponding booking ID into the map.
		 * 
		 * Observe that only checked in rooms count. 
		 */
		for (Room room : roomManager.getRooms()){
			
			if(!roomAvailable(room, date, date)){
				
				for (Booking booking : bookings) {
					
					for (RoomReservation res : booking.getReservations()) {
						
						if (res.getRoomId() == room.getRoomNumber()) {
							
							occupiedRooms.put(booking.getId(), room);
						}
					}					
				}
			}
		}
		return occupiedRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listBookings() {
		return bookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public boolean changeRoomType(int bookingId, String fromRoomType, String toRoomType) {
		Booking b = getBooking(bookingId);
		RoomType to = roomManager.getRoomType(toRoomType);
		RoomType from = roomManager.getRoomType(fromRoomType);
		for(RoomReservation rr : b.getReservations()){
			if(rr.getRoomType().equals(from)){
				if(roomTypeAvailable(to, b.getStartDate(),b.getEndDate())!=null){
					rr.setRoomType(to);
				}else{
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public boolean removeRoomFromBooking(int bookingId, String roomType) {
		Booking b = getBooking(bookingId);
		RoomType rt = roomManager.getRoomType(roomType);
		return b.removeRoom(rt);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated nop
	 */
	public boolean changeDate(int bookingId, String newStartDate, String newEndDate) {

		if(!(validDate(newStartDate,newEndDate))) return false;
		Booking b = getBooking(bookingId);
		for(RoomReservation rr : b.getReservations()){
			if(roomTypeAvailable(rr.getRoomType(), newStartDate, newEndDate)==null){
				return false;
			}
		}
		b.setStartDate(newStartDate);
		b.setEndDate(newEndDate);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated nop
	 */
	public EList<Booking> listCheckIns(String startDate, String endDate) {
		EList<Booking> checkIns = new BasicEList<Booking>();
		for(Booking b : bookings){
			if(dateBetween(b.getStartDate(), startDate, endDate)){
				checkIns.add(b);
			}
		}
		return checkIns;
	}

	private boolean dateBetween(String date, String startDate, String endDate) {
		// YYYYMMDD
		DateFormat df = new SimpleDateFormat("yyyymmdd");
		Date start = null;
		Date end = null;
		Date mainDate = null;
		try {
			start = df.parse(startDate);
			end = df.parse(endDate);
			mainDate = df.parse(date);
		} catch (ParseException e) {
			return false;
		}

		return (mainDate.equals(start)||((mainDate.after(start)) && mainDate.before(end)) || mainDate.equals(end));

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public EList<Booking> listCheckOut(String startDate, String endDate) {
		EList<Booking> checkOuts = new BasicEList<Booking>();
		for(Booking b : bookings){
			if(b.isCheckedOut() && dateBetween(b.getEndDate(), startDate, endDate)){
				checkOuts.add(b);
			}
		}
		return checkOuts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public boolean checkIn(int bookingId) {
		Booking b = getBooking(bookingId);
		if(b==null){
			return false;
		}
		b.setCheckedIn(true);
		return true;
	}
	
	@SuppressWarnings("unused")
	private int getAvailableRoom(RoomType roomType) {
		for(Room r : roomManager.getRooms()){
			if(r.getType().equals(roomType) && r.isFree()){
				return r.getRoomNumber();
			}
		}
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtraCost(int bookingId, int roomNumber, String description, double extraPrice) {
		Booking b = getBooking(bookingId);
		for(RoomReservation r : b.getReservations()){
			if(r.getRoomId() == roomNumber){
				r.addExtraCost(description, extraPrice);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public void cancelBooking(int bookingId) {

		Booking b = getBooking(bookingId);
		
		b.getReservations().clear();
		b.setConfirmed(false); // Remove instead of set confirmed not ok

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public EList<RoomReservation> getBookingRoomReservations(Booking booking) {
		return booking.getReservations();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.BOOKING_MANAGER__BOOKINGS:
				return ((InternalEList<?>)getBookings()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.BOOKING_MANAGER__BOOKINGS:
				return getBookings();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.BOOKING_MANAGER__BOOKINGS:
				getBookings().clear();
				getBookings().addAll((Collection<? extends Booking>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.BOOKING_MANAGER__BOOKINGS:
				getBookings().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.BOOKING_MANAGER__BOOKINGS:
				return bookings != null && !bookings.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == CoreBooking.class) {
			switch (baseOperationID) {
				case ClassDiagramComponentsPackage.CORE_BOOKING___CREATE_BOOKING__STRING_STRING_STRING_STRING: return ClassDiagramComponentsPackage.BOOKING_MANAGER___CREATE_BOOKING__STRING_STRING_STRING_STRING;
				case ClassDiagramComponentsPackage.CORE_BOOKING___CHECK_OUT_BOOKING__INT: return ClassDiagramComponentsPackage.BOOKING_MANAGER___CHECK_OUT_BOOKING__INT;
				case ClassDiagramComponentsPackage.CORE_BOOKING___CHECK_IN_ROOM__STRING_INT: return ClassDiagramComponentsPackage.BOOKING_MANAGER___CHECK_IN_ROOM__STRING_INT;
				case ClassDiagramComponentsPackage.CORE_BOOKING___CHECK_OUT_ROOM__INT_INT: return ClassDiagramComponentsPackage.BOOKING_MANAGER___CHECK_OUT_ROOM__INT_INT;
				case ClassDiagramComponentsPackage.CORE_BOOKING___ADD_ROOM_TO_BOOKING__INT_STRING: return ClassDiagramComponentsPackage.BOOKING_MANAGER___ADD_ROOM_TO_BOOKING__INT_STRING;
				case ClassDiagramComponentsPackage.CORE_BOOKING___CONFIRM_BOOKING__INT: return ClassDiagramComponentsPackage.BOOKING_MANAGER___CONFIRM_BOOKING__INT;
				case ClassDiagramComponentsPackage.CORE_BOOKING___GET_BOOKING_FROM_ROOM__INT: return ClassDiagramComponentsPackage.BOOKING_MANAGER___GET_BOOKING_FROM_ROOM__INT;
				case ClassDiagramComponentsPackage.CORE_BOOKING___GET_BOOKING_FROM_NAME__STRING_STRING: return ClassDiagramComponentsPackage.BOOKING_MANAGER___GET_BOOKING_FROM_NAME__STRING_STRING;
				default: return -1;
			}
		}
		if (baseClass == ListFreeRooms.class) {
			switch (baseOperationID) {
				case ClassDiagramComponentsPackage.LIST_FREE_ROOMS___LIST_FREE_ROOMS__STRING_STRING_INT: return ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_FREE_ROOMS__STRING_STRING_INT;
				default: return -1;
			}
		}
		if (baseClass == ListRooms.class) {
			switch (baseOperationID) {
				case ClassDiagramComponentsPackage.LIST_ROOMS___LIST_ROOMS: return ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_ROOMS;
				case ClassDiagramComponentsPackage.LIST_ROOMS___LIST_OCCUPIED_ROOMS__STRING: return ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_OCCUPIED_ROOMS__STRING;
				default: return -1;
			}
		}
		if (baseClass == ManageBooking.class) {
			switch (baseOperationID) {
				case ClassDiagramComponentsPackage.MANAGE_BOOKING___LIST_BOOKINGS: return ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_BOOKINGS;
				case ClassDiagramComponentsPackage.MANAGE_BOOKING___CHANGE_ROOM_TYPE__INT_STRING_STRING: return ClassDiagramComponentsPackage.BOOKING_MANAGER___CHANGE_ROOM_TYPE__INT_STRING_STRING;
				case ClassDiagramComponentsPackage.MANAGE_BOOKING___REMOVE_ROOM_FROM_BOOKING__INT_STRING: return ClassDiagramComponentsPackage.BOOKING_MANAGER___REMOVE_ROOM_FROM_BOOKING__INT_STRING;
				case ClassDiagramComponentsPackage.MANAGE_BOOKING___CHANGE_DATE__INT_STRING_STRING: return ClassDiagramComponentsPackage.BOOKING_MANAGER___CHANGE_DATE__INT_STRING_STRING;
				case ClassDiagramComponentsPackage.MANAGE_BOOKING___LIST_CHECK_INS__STRING_STRING: return ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_CHECK_INS__STRING_STRING;
				case ClassDiagramComponentsPackage.MANAGE_BOOKING___LIST_CHECK_OUT__STRING_STRING: return ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_CHECK_OUT__STRING_STRING;
				case ClassDiagramComponentsPackage.MANAGE_BOOKING___CHECK_IN__INT: return ClassDiagramComponentsPackage.BOOKING_MANAGER___CHECK_IN__INT;
				case ClassDiagramComponentsPackage.MANAGE_BOOKING___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE: return ClassDiagramComponentsPackage.BOOKING_MANAGER___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE;
				case ClassDiagramComponentsPackage.MANAGE_BOOKING___CANCEL_BOOKING__INT: return ClassDiagramComponentsPackage.BOOKING_MANAGER___CANCEL_BOOKING__INT;
				case ClassDiagramComponentsPackage.MANAGE_BOOKING___GET_BOOKING_ROOM_RESERVATIONS__BOOKING: return ClassDiagramComponentsPackage.BOOKING_MANAGER___GET_BOOKING_ROOM_RESERVATIONS__BOOKING;
				case ClassDiagramComponentsPackage.MANAGE_BOOKING___GET_BOOKING__INT: return ClassDiagramComponentsPackage.BOOKING_MANAGER___GET_BOOKING__INT;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___CLEAR_BOOKINGS:
				clearBookings();
				return null;
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___CREATE_BOOKING__STRING_STRING_STRING_STRING:
				return createBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___CHECK_OUT_BOOKING__INT:
				return checkOutBooking((Integer)arguments.get(0));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___CHECK_OUT_ROOM__INT_INT:
				return checkOutRoom((Integer)arguments.get(0), (Integer)arguments.get(1));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___ADD_ROOM_TO_BOOKING__INT_STRING:
				return addRoomToBooking((Integer)arguments.get(0), (String)arguments.get(1));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___GET_BOOKING_FROM_ROOM__INT:
				return getBookingFromRoom((Integer)arguments.get(0));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___GET_BOOKING_FROM_NAME__STRING_STRING:
				return getBookingFromName((String)arguments.get(0), (String)arguments.get(1));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_FREE_ROOMS__STRING_STRING_INT:
				return listFreeRooms((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_ROOMS:
				return listRooms();
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_OCCUPIED_ROOMS__STRING:
				return listOccupiedRooms((String)arguments.get(0));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_BOOKINGS:
				return listBookings();
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___CHANGE_ROOM_TYPE__INT_STRING_STRING:
				return changeRoomType((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___REMOVE_ROOM_FROM_BOOKING__INT_STRING:
				return removeRoomFromBooking((Integer)arguments.get(0), (String)arguments.get(1));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___CHANGE_DATE__INT_STRING_STRING:
				return changeDate((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_CHECK_INS__STRING_STRING:
				return listCheckIns((String)arguments.get(0), (String)arguments.get(1));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___LIST_CHECK_OUT__STRING_STRING:
				return listCheckOut((String)arguments.get(0), (String)arguments.get(1));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___CHECK_IN__INT:
				return checkIn((Integer)arguments.get(0));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE:
				addExtraCost((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
				return null;
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___CANCEL_BOOKING__INT:
				cancelBooking((Integer)arguments.get(0));
				return null;
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___GET_BOOKING_ROOM_RESERVATIONS__BOOKING:
				return getBookingRoomReservations((Booking)arguments.get(0));
			case ClassDiagramComponentsPackage.BOOKING_MANAGER___GET_BOOKING__INT:
				return getBooking((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //BookingManagerImpl
