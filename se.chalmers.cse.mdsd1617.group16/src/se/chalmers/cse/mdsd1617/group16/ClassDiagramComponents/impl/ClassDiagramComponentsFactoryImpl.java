/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassDiagramComponentsFactoryImpl extends EFactoryImpl implements ClassDiagramComponentsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ClassDiagramComponentsFactory init() {
		try {
			ClassDiagramComponentsFactory theClassDiagramComponentsFactory = (ClassDiagramComponentsFactory)EPackage.Registry.INSTANCE.getEFactory(ClassDiagramComponentsPackage.eNS_URI);
			if (theClassDiagramComponentsFactory != null) {
				return theClassDiagramComponentsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ClassDiagramComponentsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassDiagramComponentsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ClassDiagramComponentsPackage.FREE_ROOM_TYPES_DTO: return createFreeRoomTypesDTO();
			case ClassDiagramComponentsPackage.HOTEL_CUSTOMER_PROVIDES: return createHotelCustomerProvides();
			case ClassDiagramComponentsPackage.HOTEL_STARTUP_PROVIDES: return createHotelStartupProvides();
			case ClassDiagramComponentsPackage.EXTERNAL_SUPPLIER_SYSTEM: return createExternalSupplierSystem();
			case ClassDiagramComponentsPackage.BOOKING: return createBooking();
			case ClassDiagramComponentsPackage.ROOM_RESERVATION: return createRoomReservation();
			case ClassDiagramComponentsPackage.ROOM_TYPE: return createRoomType();
			case ClassDiagramComponentsPackage.EXTRA_COST: return createExtraCost();
			case ClassDiagramComponentsPackage.ROOM: return createRoom();
			case ClassDiagramComponentsPackage.RECEPTIONIST: return createReceptionist();
			case ClassDiagramComponentsPackage.BOOKING_MANAGER: return createBookingManager();
			case ClassDiagramComponentsPackage.ROOM_MANAGER: return createRoomManager();
			case ClassDiagramComponentsPackage.ADMIN: return createAdmin();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FreeRoomTypesDTO createFreeRoomTypesDTO() {
		FreeRoomTypesDTOImpl freeRoomTypesDTO = new FreeRoomTypesDTOImpl();
		return freeRoomTypesDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelCustomerProvides createHotelCustomerProvides() {
		HotelCustomerProvidesImpl hotelCustomerProvides = new HotelCustomerProvidesImpl();
		return hotelCustomerProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartupProvides createHotelStartupProvides() {
		HotelStartupProvidesImpl hotelStartupProvides = new HotelStartupProvidesImpl();
		return hotelStartupProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalSupplierSystem createExternalSupplierSystem() {
		ExternalSupplierSystemImpl externalSupplierSystem = new ExternalSupplierSystemImpl();
		return externalSupplierSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Receptionist createReceptionist() {
		ReceptionistImpl receptionist = new ReceptionistImpl();
		return receptionist;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Booking createBooking() {
		BookingImpl booking = new BookingImpl();
		return booking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomReservation createRoomReservation() {
		RoomReservationImpl roomReservation = new RoomReservationImpl();
		return roomReservation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingManager createBookingManager() {
		BookingManagerImpl bookingManager = new BookingManagerImpl();
		return bookingManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room createRoom() {
		RoomImpl room = new RoomImpl();
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomType createRoomType() {
		RoomTypeImpl roomType = new RoomTypeImpl();
		return roomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtraCost createExtraCost() {
		ExtraCostImpl extraCost = new ExtraCostImpl();
		return extraCost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomManager createRoomManager() {
		RoomManagerImpl roomManager = new RoomManagerImpl();
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Admin createAdmin() {
		AdminImpl admin = new AdminImpl();
		return admin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassDiagramComponentsPackage getClassDiagramComponentsPackage() {
		return (ClassDiagramComponentsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ClassDiagramComponentsPackage getPackage() {
		return ClassDiagramComponentsPackage.eINSTANCE;
	}

} //ClassDiagramComponentsFactoryImpl
