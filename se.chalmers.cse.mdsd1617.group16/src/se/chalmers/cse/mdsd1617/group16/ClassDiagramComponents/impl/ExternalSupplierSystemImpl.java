/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl;

import org.eclipse.emf.ecore.EClass;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ExternalSupplierSystem;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>External Supplier System</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExternalSupplierSystemImpl extends HotelCustomerProvidesImpl implements ExternalSupplierSystem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExternalSupplierSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassDiagramComponentsPackage.Literals.EXTERNAL_SUPPLIER_SYSTEM;
	}

} //ExternalSupplierSystemImpl
