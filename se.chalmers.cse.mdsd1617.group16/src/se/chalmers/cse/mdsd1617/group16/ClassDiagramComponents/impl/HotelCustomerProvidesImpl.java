/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl;

import java.lang.reflect.InvocationTargetException;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.HotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListFreeRooms;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class HotelCustomerProvidesImpl extends MinimalEObjectImpl.Container implements HotelCustomerProvides {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	protected HotelCustomerProvidesImpl() {
		super();
		BookingManagerImpl bm = BookingManagerImpl.getBookingManagerImpl(); 
		this.roomLister = bm;
		this.bookingManager = bm;
		currentCheckout = -1;
	}

	private ListFreeRooms roomLister;
	private CoreBooking bookingManager;
	private se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires bankingManager;
	private int currentCheckout; // -1 means that there is no current checkout
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated nono
	 */		
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassDiagramComponentsPackage.Literals.HOTEL_CUSTOMER_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated notttt
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		return roomLister.listFreeRooms(startDate, endDate, numBeds);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		return bookingManager.createBooking(firstName, lastName, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		return bookingManager.addRoomToBooking(bookingID, roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean confirmBooking(int bookingID) {
		return bookingManager.confirmBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public double initiateCheckout(int bookingID) {
		if (currentCheckout > -1) {
			return 0;
		}
		currentCheckout = bookingID; // Have to make sure it's set back to -1 after done with checkout
		double result = bookingManager.checkOutBooking(bookingID); // this is total cost of entire booking!
		if (result == -1) {
			result = 0;
			currentCheckout = -1;
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		double sum = bookingManager.checkOutBooking(currentCheckout);
		if(sum<0) {
			return false; //Invalid sum
		}
		
		try {
			if(this.bankingManager == null){
				this.bankingManager = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
			}
			boolean result = bankingManager.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, sum);
			if (result)
				bookingManager.getBookingFromName(firstName, lastName).setIsPayed(true);
				currentCheckout = -1;
			
			return result;
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no this is patrick
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingID) {
		if (currentCheckout > -1) {
			return 0;
		}
		currentCheckout = bookingID; // Have to make sure it's set back to -1 after done with checkout
		double result = bookingManager.checkOutRoom(roomNumber, bookingID); // this is total cost of entire booking!
		if (result == -1) {
			result = 0;
			currentCheckout = -1;
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName){
		if (currentCheckout == -1 ) {
			return false;
		}
		
		try {
			this.bankingManager = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try{
			if (bankingManager.isCreditCardValid("00000000", "000", 11, 16, "Peter", "Pan")) {
				System.out.println("Valid credit card");
			} else {
				System.out.println("Invalid credit card");
				return false;
			}
		} catch (SOAPException e) {
			e.printStackTrace();
			return false;
		}
		
		
		double sum = bookingManager.checkOutRoom(roomNumber, currentCheckout);
		System.out.println("Sum: " + sum);
		if(sum<0)
			return false; //Invalid sum
		
		try {
			boolean result = bankingManager.makePayment(ccNumber, ccv,expiryMonth, expiryYear, firstName,lastName, sum);
			if (result) {
				
				double curPaid = bookingManager.getBookingFromName(firstName, lastName).getAmountPaid();
				
				bookingManager.getBookingFromName(firstName, lastName).setAmountPaid(curPaid+sum);
				
				curPaid = bookingManager.getBookingFromName(firstName, lastName).getAmountPaid();
				
				if(curPaid == bookingManager.getBookingFromName(firstName, lastName).getTotalCost()) {
					bookingManager.getBookingFromName(firstName, lastName).setCheckedOut(true);
					bookingManager.getBookingFromName(firstName, lastName).setIsPayed(true);
				}
				
				currentCheckout = -1;
			}
			
			return result;
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public int checkInRoom(String roomTypeDescription, int bookingId) {
		return bookingManager.checkInRoom(roomTypeDescription, bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ClassDiagramComponentsPackage.HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case ClassDiagramComponentsPackage.HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case ClassDiagramComponentsPackage.HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case ClassDiagramComponentsPackage.HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case ClassDiagramComponentsPackage.HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case ClassDiagramComponentsPackage.HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case ClassDiagramComponentsPackage.HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case ClassDiagramComponentsPackage.HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case ClassDiagramComponentsPackage.HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelCustomerProvidesImpl
