/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClearBooking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.HotelStartupProvides;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class HotelStartupProvidesImpl extends MinimalEObjectImpl.Container implements HotelStartupProvides {
	
	private ClearBooking bManager;
	private AddClearManager rManager;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated s
	 */
	public HotelStartupProvidesImpl() {
		super();
		this.bManager = BookingManagerImpl.getBookingManagerImpl();	
		this.rManager = RoomManagerImpl.getRoomManagerImpl();
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassDiagramComponentsPackage.Literals.HOTEL_STARTUP_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		
	
		//Clear all bookings, rooms, and room types that already exist in the system
		bManager.clearBookings();
		rManager.clearRooms();
		rManager.clearRoomTypes();
		
		//Add a new type of room with two beds
		RoomType rt = rManager.addRoomType("Double", 850, 2, "");
		

		//Add new rooms, with two beds each
		
		for(int i = 0; i < numRooms; i++) {
			rManager.addRoom(i, rt);
			
		}
		
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ClassDiagramComponentsPackage.HOTEL_STARTUP_PROVIDES___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelStartupProvidesImpl
