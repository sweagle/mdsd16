/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Receptionist</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ReceptionistImpl extends HotelCustomerProvidesImpl implements Receptionist {
	private ManageBooking bManager;
	private ListRooms roomLister;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated asd
	 */
	

	public ReceptionistImpl() {
		super();
		bManager = BookingManagerImpl.getBookingManagerImpl();
		roomLister = BookingManagerImpl.getBookingManagerImpl();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassDiagramComponentsPackage.Literals.RECEPTIONIST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean changeDate(int bookingId, String newStartDate, String newEndDate) {
		return bManager.changeDate(bookingId, newStartDate, newEndDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public EList<Booking> listBookings() {
		return bManager.listBookings();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated nit
	 */
	public EList<Booking> listCheckIns(String startDate, String endDate) {
		return bManager.listCheckIns(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated asd
	 */
	public boolean checkIn(int bookingId) {
		return bManager.checkIn(bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated aasd
	 */
	public boolean removeRoomFromBooking(int bookingId, String roomType) {
		return bManager.removeRoomFromBooking(bookingId, roomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated asd
	 */
	public EList<RoomReservation> getBookingRoomReservations(Booking booking) {
		return bManager.getBookingRoomReservations(booking);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated asda
	 */
	public EList<Booking> listCheckOut(String startDate, String endDate) {
		return bManager.listCheckOut(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated asd
	 */
	public void cancelBooking(int bookingId) {
		bManager.cancelBooking(bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated asd
	 */
	public Booking getBooking(int bookingId) {
		return bManager.getBooking(bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated asd
	 */
	public boolean changeRoomType(int bookingId, String fromRoomType, String toRoomType) {
		return bManager.changeRoomType(bookingId, fromRoomType, toRoomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated asd
	 */
	public void addExtraCost(int bookingId, int roomNumber, String description, double extraPrice) {
		bManager.addExtraCost(bookingId, roomNumber, description, extraPrice);
	}
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no, this is Patrick
	 */
	public Map<Integer, Room> listOccupiedRooms(String date) {
		
		return roomLister.listOccupiedRooms(date);
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ClassDiagramComponentsPackage.RECEPTIONIST___CHANGE_DATE__INT_STRING_STRING:
				return changeDate((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case ClassDiagramComponentsPackage.RECEPTIONIST___LIST_BOOKINGS:
				return listBookings();
			case ClassDiagramComponentsPackage.RECEPTIONIST___LIST_CHECK_INS__STRING_STRING:
				return listCheckIns((String)arguments.get(0), (String)arguments.get(1));
			case ClassDiagramComponentsPackage.RECEPTIONIST___CHECK_IN__INT:
				return checkIn((Integer)arguments.get(0));
			case ClassDiagramComponentsPackage.RECEPTIONIST___REMOVE_ROOM_FROM_BOOKING__INT_STRING:
				return removeRoomFromBooking((Integer)arguments.get(0), (String)arguments.get(1));
			case ClassDiagramComponentsPackage.RECEPTIONIST___GET_BOOKING_ROOM_RESERVATIONS__BOOKING:
				return getBookingRoomReservations((Booking)arguments.get(0));
			case ClassDiagramComponentsPackage.RECEPTIONIST___LIST_CHECK_OUT__STRING_STRING:
				return listCheckOut((String)arguments.get(0), (String)arguments.get(1));
			case ClassDiagramComponentsPackage.RECEPTIONIST___CANCEL_BOOKING__INT:
				cancelBooking((Integer)arguments.get(0));
				return null;
			case ClassDiagramComponentsPackage.RECEPTIONIST___GET_BOOKING__INT:
				return getBooking((Integer)arguments.get(0));
			case ClassDiagramComponentsPackage.RECEPTIONIST___CHANGE_ROOM_TYPE__INT_STRING_STRING:
				return changeRoomType((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case ClassDiagramComponentsPackage.RECEPTIONIST___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE:
				addExtraCost((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
				return null;
			case ClassDiagramComponentsPackage.RECEPTIONIST___LIST_OCCUPIED_ROOMS__STRING:
				return listOccupiedRooms((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ReceptionistImpl
