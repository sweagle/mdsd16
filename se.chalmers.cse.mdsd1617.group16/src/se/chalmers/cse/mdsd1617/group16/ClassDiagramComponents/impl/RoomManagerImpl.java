/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomManagerImpl#getRooms <em>Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomManagerImpl#getRoomTypes <em>Room Types</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomManagerImpl extends MinimalEObjectImpl.Container implements RoomManager {
	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> rooms;

	/**
	 * The cached value of the '{@link #getRoomTypes() <em>Room Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<RoomType> roomTypes;
	private static RoomManager theRoomManager;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomManagerImpl() {
		super();
		rooms = new BasicEList<Room>();
		roomTypes = new BasicEList<RoomType>();
	}
	
	public static RoomManagerImpl getRoomManagerImpl(){
		if(theRoomManager == null){
			theRoomManager = new RoomManagerImpl();
		}
		
		return (RoomManagerImpl) theRoomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassDiagramComponentsPackage.Literals.ROOM_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public EList<Room> getRooms() {
		if (rooms == null) {
			rooms = new EObjectContainmentEList<Room>(Room.class, this, ClassDiagramComponentsPackage.ROOM_MANAGER__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public EList<RoomType> getRoomTypes() {
		if (roomTypes == null) {
			roomTypes = new EObjectContainmentEList<RoomType>(RoomType.class, this, ClassDiagramComponentsPackage.ROOM_MANAGER__ROOM_TYPES);
		}
		return roomTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NIET
	 */
	public RoomType addRoomType(String name, double price, int numOfBeds, String additionalFeatures) {
		RoomType roomType = new RoomTypeImpl(); 
		roomType.setType(name);
		roomType.setCost(price);
		roomType.setNumberOfBeds(numOfBeds);
		roomType.setAdditionalFeatures(additionalFeatures);
		
		roomTypes.add(roomType);
		return roomType; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NONO
	 */
	public Room addRoom(int roomNumber, RoomType roomType) {
		Room room = new RoomImpl();
		room.setRoomNumber(roomNumber);
		room.setType(roomType);
	
		rooms.add(room);
		return room; //rooms.add(room);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated JK XD
	 */
	public void clearRooms() {
		rooms.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated Nonononono
	 */
	public void clearRoomTypes() {
		roomTypes.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void changeRoomType(int roomNumber, RoomType roomType) {
		Room room = null;
		for (int i = 0; i < rooms.size(); i++) {
			room = rooms.get(i);
			if (room.getRoomNumber() == roomNumber) {
				room.setType(roomType);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void removeRoom(int roomNumber) {
		Room room;
		for(int i = 0; i < rooms.size(); i++){
			room = rooms.get(i);
			if(room.getRoomNumber() == roomNumber){
				rooms.remove(room);
				break;
			}
		}
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void blockRoom(int roomNumber) {
		for(Room room : rooms){
			if(room.getRoomNumber() == roomNumber){
				room.setIsBlocked(true);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated ne
	 */
	public void unblockRoom(int roomNumber) {
		for(Room room : rooms){
			if(room.getRoomNumber() == roomNumber){
				room.setIsBlocked(false);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated ne
	 */
	public void updateRoomType(String name, double price, int numberOfBeds, String additionalFeatures) {
		for(RoomType type : roomTypes){
			if(type.getType() == name){
				type.setCost(price);
				type.setNumberOfBeds(numberOfBeds);
				type.setAdditionalFeatures(additionalFeatures);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated ne 
	 */
	public boolean removeRoomType(String name) {
		
		
		for(RoomType type : roomTypes){
			if(type.getType().equals(name)){
				
				for(Room r : rooms){
					if(r.getType().getType().equals(name)){
						return false; // a room is having that roomType. RT should not be removed.
					}
				}
				
				roomTypes.remove(roomTypes.indexOf(type));
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public RoomType getRoomType(String roomTypeDescription) {
		for(RoomType rt:roomTypes){
			if(rt.getType().equals(roomTypeDescription)){
				return rt;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.ROOM_MANAGER__ROOMS:
				return ((InternalEList<?>)getRooms()).basicRemove(otherEnd, msgs);
			case ClassDiagramComponentsPackage.ROOM_MANAGER__ROOM_TYPES:
				return ((InternalEList<?>)getRoomTypes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.ROOM_MANAGER__ROOMS:
				return getRooms();
			case ClassDiagramComponentsPackage.ROOM_MANAGER__ROOM_TYPES:
				return getRoomTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.ROOM_MANAGER__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Room>)newValue);
				return;
			case ClassDiagramComponentsPackage.ROOM_MANAGER__ROOM_TYPES:
				getRoomTypes().clear();
				getRoomTypes().addAll((Collection<? extends RoomType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.ROOM_MANAGER__ROOMS:
				getRooms().clear();
				return;
			case ClassDiagramComponentsPackage.ROOM_MANAGER__ROOM_TYPES:
				getRoomTypes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.ROOM_MANAGER__ROOMS:
				return rooms != null && !rooms.isEmpty();
			case ClassDiagramComponentsPackage.ROOM_MANAGER__ROOM_TYPES:
				return roomTypes != null && !roomTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ClassDiagramComponentsPackage.ROOM_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				return addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
			case ClassDiagramComponentsPackage.ROOM_MANAGER___ADD_ROOM__INT_ROOMTYPE:
				return addRoom((Integer)arguments.get(0), (RoomType)arguments.get(1));
			case ClassDiagramComponentsPackage.ROOM_MANAGER___CLEAR_ROOMS:
				clearRooms();
				return null;
			case ClassDiagramComponentsPackage.ROOM_MANAGER___CLEAR_ROOM_TYPES:
				clearRoomTypes();
				return null;
			case ClassDiagramComponentsPackage.ROOM_MANAGER___CHANGE_ROOM_TYPE__INT_ROOMTYPE:
				changeRoomType((Integer)arguments.get(0), (RoomType)arguments.get(1));
				return null;
			case ClassDiagramComponentsPackage.ROOM_MANAGER___REMOVE_ROOM__INT:
				removeRoom((Integer)arguments.get(0));
				return null;
			case ClassDiagramComponentsPackage.ROOM_MANAGER___BLOCK_ROOM__INT:
				blockRoom((Integer)arguments.get(0));
				return null;
			case ClassDiagramComponentsPackage.ROOM_MANAGER___UNBLOCK_ROOM__INT:
				unblockRoom((Integer)arguments.get(0));
				return null;
			case ClassDiagramComponentsPackage.ROOM_MANAGER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				updateRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
				return null;
			case ClassDiagramComponentsPackage.ROOM_MANAGER___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case ClassDiagramComponentsPackage.ROOM_MANAGER___GET_ROOM_TYPE__STRING:
				return getRoomType((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomManagerImpl
