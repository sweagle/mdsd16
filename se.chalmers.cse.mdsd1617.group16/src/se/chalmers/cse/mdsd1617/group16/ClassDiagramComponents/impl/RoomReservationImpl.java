/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ExtraCost;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room Reservation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomReservationImpl#getRoomId <em>Room Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomReservationImpl#getBooking <em>Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomReservationImpl#getRoomType <em>Room Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomReservationImpl#getExtraCost <em>Extra Cost</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomReservationImpl#isCheckedOut <em>Is Checked Out</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomReservationImpl extends MinimalEObjectImpl.Container implements RoomReservation {
	/**
	 * The default value of the '{@link #getRoomId() <em>Room Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomId()
	 * @generated
	 * @ordered
	 */
	protected static final int ROOM_ID_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getRoomId() <em>Room Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomId()
	 * @generated
	 * @ordered
	 */
	protected int roomId = ROOM_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBooking() <em>Booking</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBooking()
	 * @generated
	 * @ordered
	 */
	protected Booking booking;

	/**
	 * The cached value of the '{@link #getRoomType() <em>Room Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomType()
	 * @generated
	 * @ordered
	 */
	protected RoomType roomType;

	/**
	 * The cached value of the '{@link #getExtraCost() <em>Extra Cost</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtraCost()
	 * @generated
	 * @ordered
	 */
	protected EList<ExtraCost> extraCost;

	/**
	 * The default value of the '{@link #isCheckedOut() <em>Is Checked Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCheckedOut()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_CHECKED_OUT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCheckedOut() <em>Is Checked Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCheckedOut()
	 * @generated
	 * @ordered
	 */
	protected boolean isCheckedOut = IS_CHECKED_OUT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	protected RoomReservationImpl() {
		super();
		extraCost = new BasicEList<ExtraCost>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassDiagramComponentsPackage.Literals.ROOM_RESERVATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRoomId() {
		return roomId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setRoomId(int newRoomId) {
		
		roomId = newRoomId;
	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public Booking getBooking() {
		return booking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public NotificationChain basicSetBooking(Booking newBooking, NotificationChain msgs) {
		
		booking = newBooking;
	
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setBooking(Booking newBooking) {
		booking = newBooking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomType getRoomType() {
		return roomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public NotificationChain basicSetRoomType(RoomType newRoomType, NotificationChain msgs) {
		
		roomType = newRoomType;
		
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setRoomType(RoomType newRoomType) {
		
		roomType = newRoomType;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExtraCost> getExtraCost() {
		if (extraCost == null) {
			extraCost = new EObjectContainmentEList<ExtraCost>(ExtraCost.class, this, ClassDiagramComponentsPackage.ROOM_RESERVATION__EXTRA_COST);
		}
		return extraCost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCheckedOut() {
		return isCheckedOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setIsCheckedOut(boolean newIsCheckedOut) {
		
		isCheckedOut = newIsCheckedOut;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated nooo
	 */
	public double getTotalCost() {
		double sum = roomType.getCost();
		for(ExtraCost e : extraCost){
			sum+=e.getCost();
		}
		return sum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public void addExtraCost(String description, double cost) {
		ExtraCost extra = new ExtraCostImpl();
		extra.setDescription(description);
		extra.setCost(cost);
		extraCost.add(extra);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated no
	 */
	public void checkOut() {
		isCheckedOut = true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__BOOKING:
				return basicSetBooking(null, msgs);
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__ROOM_TYPE:
				return basicSetRoomType(null, msgs);
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__EXTRA_COST:
				return ((InternalEList<?>)getExtraCost()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__ROOM_ID:
				return getRoomId();
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__BOOKING:
				return getBooking();
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__ROOM_TYPE:
				return getRoomType();
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__EXTRA_COST:
				return getExtraCost();
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__IS_CHECKED_OUT:
				return isCheckedOut();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__ROOM_ID:
				setRoomId((Integer)newValue);
				return;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__BOOKING:
				setBooking((Booking)newValue);
				return;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__ROOM_TYPE:
				setRoomType((RoomType)newValue);
				return;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__EXTRA_COST:
				getExtraCost().clear();
				getExtraCost().addAll((Collection<? extends ExtraCost>)newValue);
				return;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__IS_CHECKED_OUT:
				setIsCheckedOut((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__ROOM_ID:
				setRoomId(ROOM_ID_EDEFAULT);
				return;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__BOOKING:
				setBooking((Booking)null);
				return;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__ROOM_TYPE:
				setRoomType((RoomType)null);
				return;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__EXTRA_COST:
				getExtraCost().clear();
				return;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__IS_CHECKED_OUT:
				setIsCheckedOut(IS_CHECKED_OUT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__ROOM_ID:
				return roomId != ROOM_ID_EDEFAULT;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__BOOKING:
				return booking != null;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__ROOM_TYPE:
				return roomType != null;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__EXTRA_COST:
				return extraCost != null && !extraCost.isEmpty();
			case ClassDiagramComponentsPackage.ROOM_RESERVATION__IS_CHECKED_OUT:
				return isCheckedOut != IS_CHECKED_OUT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ClassDiagramComponentsPackage.ROOM_RESERVATION___GET_TOTAL_COST:
				return getTotalCost();
			case ClassDiagramComponentsPackage.ROOM_RESERVATION___ADD_EXTRA_COST__STRING_DOUBLE:
				addExtraCost((String)arguments.get(0), (Double)arguments.get(1));
				return null;
			case ClassDiagramComponentsPackage.ROOM_RESERVATION___CHECK_OUT:
				checkOut();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (roomId: ");
		result.append(roomId);
		result.append(", isCheckedOut: ");
		result.append(isCheckedOut);
		result.append(')');
		return result.toString();
	}

} //RoomReservationImpl
