/**
 */
package se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage
 * @generated
 */
public class ClassDiagramComponentsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ClassDiagramComponentsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassDiagramComponentsSwitch() {
		if (modelPackage == null) {
			modelPackage = ClassDiagramComponentsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES: {
				IHotelCustomerProvides iHotelCustomerProvides = (IHotelCustomerProvides)theEObject;
				T result = caseIHotelCustomerProvides(iHotelCustomerProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.FREE_ROOM_TYPES_DTO: {
				FreeRoomTypesDTO freeRoomTypesDTO = (FreeRoomTypesDTO)theEObject;
				T result = caseFreeRoomTypesDTO(freeRoomTypesDTO);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.IHOTEL_STARTUP_PROVIDES: {
				IHotelStartupProvides iHotelStartupProvides = (IHotelStartupProvides)theEObject;
				T result = caseIHotelStartupProvides(iHotelStartupProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.HOTEL_CUSTOMER_PROVIDES: {
				HotelCustomerProvides hotelCustomerProvides = (HotelCustomerProvides)theEObject;
				T result = caseHotelCustomerProvides(hotelCustomerProvides);
				if (result == null) result = caseIHotelCustomerProvides(hotelCustomerProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.HOTEL_STARTUP_PROVIDES: {
				HotelStartupProvides hotelStartupProvides = (HotelStartupProvides)theEObject;
				T result = caseHotelStartupProvides(hotelStartupProvides);
				if (result == null) result = caseIHotelStartupProvides(hotelStartupProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.EXTERNAL_SUPPLIER_SYSTEM: {
				ExternalSupplierSystem externalSupplierSystem = (ExternalSupplierSystem)theEObject;
				T result = caseExternalSupplierSystem(externalSupplierSystem);
				if (result == null) result = caseHotelCustomerProvides(externalSupplierSystem);
				if (result == null) result = caseIHotelCustomerProvides(externalSupplierSystem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.CORE_BOOKING: {
				CoreBooking coreBooking = (CoreBooking)theEObject;
				T result = caseCoreBooking(coreBooking);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.BOOKING: {
				Booking booking = (Booking)theEObject;
				T result = caseBooking(booking);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.ROOM_RESERVATION: {
				RoomReservation roomReservation = (RoomReservation)theEObject;
				T result = caseRoomReservation(roomReservation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.ROOM_TYPE: {
				RoomType roomType = (RoomType)theEObject;
				T result = caseRoomType(roomType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.EXTRA_COST: {
				ExtraCost extraCost = (ExtraCost)theEObject;
				T result = caseExtraCost(extraCost);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.ROOM: {
				Room room = (Room)theEObject;
				T result = caseRoom(room);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.RECEPTIONIST: {
				Receptionist receptionist = (Receptionist)theEObject;
				T result = caseReceptionist(receptionist);
				if (result == null) result = caseHotelCustomerProvides(receptionist);
				if (result == null) result = caseIHotelCustomerProvides(receptionist);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.MANAGE_BOOKING: {
				ManageBooking manageBooking = (ManageBooking)theEObject;
				T result = caseManageBooking(manageBooking);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.BOOKING_MANAGER: {
				BookingManager bookingManager = (BookingManager)theEObject;
				T result = caseBookingManager(bookingManager);
				if (result == null) result = caseClearBooking(bookingManager);
				if (result == null) result = caseCoreBooking(bookingManager);
				if (result == null) result = caseListRooms(bookingManager);
				if (result == null) result = caseManageBooking(bookingManager);
				if (result == null) result = caseListFreeRooms(bookingManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.CLEAR_BOOKING: {
				ClearBooking clearBooking = (ClearBooking)theEObject;
				T result = caseClearBooking(clearBooking);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.LIST_ROOMS: {
				ListRooms listRooms = (ListRooms)theEObject;
				T result = caseListRooms(listRooms);
				if (result == null) result = caseListFreeRooms(listRooms);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.LIST_FREE_ROOMS: {
				ListFreeRooms listFreeRooms = (ListFreeRooms)theEObject;
				T result = caseListFreeRooms(listFreeRooms);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.ROOM_MANAGER: {
				RoomManager roomManager = (RoomManager)theEObject;
				T result = caseRoomManager(roomManager);
				if (result == null) result = caseManageRoomManager(roomManager);
				if (result == null) result = caseAddClearManager(roomManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.MANAGE_ROOM_MANAGER: {
				ManageRoomManager manageRoomManager = (ManageRoomManager)theEObject;
				T result = caseManageRoomManager(manageRoomManager);
				if (result == null) result = caseAddClearManager(manageRoomManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.ADD_CLEAR_MANAGER: {
				AddClearManager addClearManager = (AddClearManager)theEObject;
				T result = caseAddClearManager(addClearManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassDiagramComponentsPackage.ADMIN: {
				Admin admin = (Admin)theEObject;
				T result = caseAdmin(admin);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IHotel Customer Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IHotel Customer Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIHotelCustomerProvides(IHotelCustomerProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Free Room Types DTO</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Free Room Types DTO</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFreeRoomTypesDTO(FreeRoomTypesDTO object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IHotel Startup Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IHotel Startup Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIHotelStartupProvides(IHotelStartupProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hotel Customer Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hotel Customer Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHotelCustomerProvides(HotelCustomerProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hotel Startup Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hotel Startup Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHotelStartupProvides(HotelStartupProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>External Supplier System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Supplier System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExternalSupplierSystem(ExternalSupplierSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Core Booking</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Core Booking</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCoreBooking(CoreBooking object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Receptionist</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Receptionist</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReceptionist(Receptionist object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Manage Booking</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Manage Booking</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseManageBooking(ManageBooking object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Booking</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Booking</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooking(Booking object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Room Reservation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Room Reservation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoomReservation(RoomReservation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Booking Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Booking Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBookingManager(BookingManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Clear Booking</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Clear Booking</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClearBooking(ClearBooking object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Room</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Room</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoom(Room object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Room Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Room Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoomType(RoomType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Extra Cost</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extra Cost</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExtraCost(ExtraCost object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Room Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Room Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoomManager(RoomManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>List Rooms</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>List Rooms</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseListRooms(ListRooms object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>List Free Rooms</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>List Free Rooms</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseListFreeRooms(ListFreeRooms object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Manage Room Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Manage Room Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseManageRoomManager(ManageRoomManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Add Clear Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Add Clear Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAddClearManager(AddClearManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Admin</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Admin</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdmin(Admin object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ClassDiagramComponentsSwitch
