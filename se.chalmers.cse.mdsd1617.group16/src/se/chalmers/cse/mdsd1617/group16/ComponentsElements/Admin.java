/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Admin</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage#getAdmin()
 * @model
 * @generated
 */
public interface Admin extends EObject {
} // Admin
