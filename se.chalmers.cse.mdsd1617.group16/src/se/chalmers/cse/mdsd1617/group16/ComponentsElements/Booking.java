/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClearBooking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageBooking;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage#getBooking()
 * @model
 * @generated
 */
public interface Booking extends ManageBooking, CoreBooking, ClearBooking {
} // Booking
