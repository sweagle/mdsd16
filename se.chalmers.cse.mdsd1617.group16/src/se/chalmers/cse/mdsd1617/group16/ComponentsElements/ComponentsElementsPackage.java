/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsFactory
 * @model kind="package"
 * @generated
 */
public interface ComponentsElementsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ComponentsElements";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group16/ComponentsElements.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group16.ComponentsElements";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ComponentsElementsPackage eINSTANCE = se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.HotelCustomerImpl <em>Hotel Customer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.HotelCustomerImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getHotelCustomer()
	 * @generated
	 */
	int HOTEL_CUSTOMER = 0;

	/**
	 * The number of structural features of the '<em>Hotel Customer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_FEATURE_COUNT = ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER___GET_FREE_ROOMS__INT_STRING_STRING = ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER___ADD_ROOM_TO_BOOKING__STRING_INT = ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER___CONFIRM_BOOKING__INT = ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER___INITIATE_CHECKOUT__INT = ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER___INITIATE_ROOM_CHECKOUT__INT_INT = ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER___CHECK_IN_ROOM__STRING_INT = ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The number of operations of the '<em>Hotel Customer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_OPERATION_COUNT = ClassDiagramComponentsPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ExternalSupplierSystemImpl <em>External Supplier System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ExternalSupplierSystemImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getExternalSupplierSystem()
	 * @generated
	 */
	int EXTERNAL_SUPPLIER_SYSTEM = 1;

	/**
	 * The number of structural features of the '<em>External Supplier System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>External Supplier System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLIER_SYSTEM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ReceptionistImpl <em>Receptionist</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ReceptionistImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getReceptionist()
	 * @generated
	 */
	int RECEPTIONIST = 2;

	/**
	 * The number of structural features of the '<em>Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.RoomManagerImpl <em>Room Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.RoomManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getRoomManager()
	 * @generated
	 */
	int ROOM_MANAGER = 3;

	/**
	 * The number of structural features of the '<em>Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_FEATURE_COUNT = ClassDiagramComponentsPackage.LIST_ROOMS_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>List Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___LIST_FREE_ROOMS__STRING_STRING_INT = ClassDiagramComponentsPackage.LIST_ROOMS___LIST_FREE_ROOMS__STRING_STRING_INT;

	/**
	 * The operation id for the '<em>List Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___LIST_ROOMS = ClassDiagramComponentsPackage.LIST_ROOMS___LIST_ROOMS;

	/**
	 * The operation id for the '<em>List Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___LIST_OCCUPIED_ROOMS__STRING = ClassDiagramComponentsPackage.LIST_ROOMS___LIST_OCCUPIED_ROOMS__STRING;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___ADD_ROOM__INT_ROOMTYPE = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Clear Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CLEAR_ROOMS = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Clear Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CLEAR_ROOM_TYPES = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CHANGE_ROOM_TYPE__INT_ROOMTYPE = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___REMOVE_ROOM__INT = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___BLOCK_ROOM__INT = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___UNBLOCK_ROOM__INT = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___REMOVE_ROOM_TYPE__STRING = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_ROOM_TYPE__STRING = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 10;

	/**
	 * The number of operations of the '<em>Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_OPERATION_COUNT = ClassDiagramComponentsPackage.LIST_ROOMS_OPERATION_COUNT + 11;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.HotelStartupImpl <em>Hotel Startup</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.HotelStartupImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getHotelStartup()
	 * @generated
	 */
	int HOTEL_STARTUP = 4;

	/**
	 * The number of structural features of the '<em>Hotel Startup</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_FEATURE_COUNT = ClassDiagramComponentsPackage.IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP___STARTUP__INT = ClassDiagramComponentsPackage.IHOTEL_STARTUP_PROVIDES___STARTUP__INT;

	/**
	 * The number of operations of the '<em>Hotel Startup</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_OPERATION_COUNT = ClassDiagramComponentsPackage.IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.BookingImpl <em>Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.BookingImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getBooking()
	 * @generated
	 */
	int BOOKING = 5;

	/**
	 * The number of structural features of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_FEATURE_COUNT = ClassDiagramComponentsPackage.MANAGE_BOOKING_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___LIST_BOOKINGS = ClassDiagramComponentsPackage.MANAGE_BOOKING___LIST_BOOKINGS;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHANGE_ROOM_TYPE__INT_STRING_STRING = ClassDiagramComponentsPackage.MANAGE_BOOKING___CHANGE_ROOM_TYPE__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___REMOVE_ROOM_FROM_BOOKING__INT_STRING = ClassDiagramComponentsPackage.MANAGE_BOOKING___REMOVE_ROOM_FROM_BOOKING__INT_STRING;

	/**
	 * The operation id for the '<em>Change Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHANGE_DATE__INT_STRING_STRING = ClassDiagramComponentsPackage.MANAGE_BOOKING___CHANGE_DATE__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___LIST_CHECK_INS__STRING_STRING = ClassDiagramComponentsPackage.MANAGE_BOOKING___LIST_CHECK_INS__STRING_STRING;

	/**
	 * The operation id for the '<em>List Check Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___LIST_CHECK_OUT__STRING_STRING = ClassDiagramComponentsPackage.MANAGE_BOOKING___LIST_CHECK_OUT__STRING_STRING;

	/**
	 * The operation id for the '<em>Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHECK_IN__INT = ClassDiagramComponentsPackage.MANAGE_BOOKING___CHECK_IN__INT;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE = ClassDiagramComponentsPackage.MANAGE_BOOKING___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CANCEL_BOOKING__INT = ClassDiagramComponentsPackage.MANAGE_BOOKING___CANCEL_BOOKING__INT;

	/**
	 * The operation id for the '<em>Get Booking Room Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_BOOKING_ROOM_RESERVATIONS__BOOKING = ClassDiagramComponentsPackage.MANAGE_BOOKING___GET_BOOKING_ROOM_RESERVATIONS__BOOKING;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_BOOKING__INT = ClassDiagramComponentsPackage.MANAGE_BOOKING___GET_BOOKING__INT;

	/**
	 * The operation id for the '<em>Create Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CREATE_BOOKING__STRING_STRING_STRING_STRING = ClassDiagramComponentsPackage.MANAGE_BOOKING_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHECK_OUT_BOOKING__INT = ClassDiagramComponentsPackage.MANAGE_BOOKING_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHECK_IN_ROOM__STRING_INT = ClassDiagramComponentsPackage.MANAGE_BOOKING_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHECK_OUT_ROOM__INT_INT = ClassDiagramComponentsPackage.MANAGE_BOOKING_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_ROOM_TO_BOOKING__INT_STRING = ClassDiagramComponentsPackage.MANAGE_BOOKING_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CONFIRM_BOOKING__INT = ClassDiagramComponentsPackage.MANAGE_BOOKING_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Booking From Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_BOOKING_FROM_ROOM__INT = ClassDiagramComponentsPackage.MANAGE_BOOKING_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Booking From Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_BOOKING_FROM_NAME__STRING_STRING = ClassDiagramComponentsPackage.MANAGE_BOOKING_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Clear Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CLEAR_BOOKINGS = ClassDiagramComponentsPackage.MANAGE_BOOKING_OPERATION_COUNT + 8;

	/**
	 * The number of operations of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_OPERATION_COUNT = ClassDiagramComponentsPackage.MANAGE_BOOKING_OPERATION_COUNT + 9;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.AdminImpl <em>Admin</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.AdminImpl
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getAdmin()
	 * @generated
	 */
	int ADMIN = 6;

	/**
	 * The number of structural features of the '<em>Admin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Admin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.HotelCustomer <em>Hotel Customer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Customer</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.HotelCustomer
	 * @generated
	 */
	EClass getHotelCustomer();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.ExternalSupplierSystem <em>External Supplier System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Supplier System</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.ExternalSupplierSystem
	 * @generated
	 */
	EClass getExternalSupplierSystem();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.Receptionist <em>Receptionist</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Receptionist</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.Receptionist
	 * @generated
	 */
	EClass getReceptionist();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.RoomManager <em>Room Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.RoomManager
	 * @generated
	 */
	EClass getRoomManager();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.HotelStartup <em>Hotel Startup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Startup</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.HotelStartup
	 * @generated
	 */
	EClass getHotelStartup();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.Booking
	 * @generated
	 */
	EClass getBooking();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.Admin <em>Admin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Admin</em>'.
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.Admin
	 * @generated
	 */
	EClass getAdmin();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ComponentsElementsFactory getComponentsElementsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.HotelCustomerImpl <em>Hotel Customer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.HotelCustomerImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getHotelCustomer()
		 * @generated
		 */
		EClass HOTEL_CUSTOMER = eINSTANCE.getHotelCustomer();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ExternalSupplierSystemImpl <em>External Supplier System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ExternalSupplierSystemImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getExternalSupplierSystem()
		 * @generated
		 */
		EClass EXTERNAL_SUPPLIER_SYSTEM = eINSTANCE.getExternalSupplierSystem();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ReceptionistImpl <em>Receptionist</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ReceptionistImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getReceptionist()
		 * @generated
		 */
		EClass RECEPTIONIST = eINSTANCE.getReceptionist();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.RoomManagerImpl <em>Room Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.RoomManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getRoomManager()
		 * @generated
		 */
		EClass ROOM_MANAGER = eINSTANCE.getRoomManager();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.HotelStartupImpl <em>Hotel Startup</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.HotelStartupImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getHotelStartup()
		 * @generated
		 */
		EClass HOTEL_STARTUP = eINSTANCE.getHotelStartup();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.BookingImpl <em>Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.BookingImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getBooking()
		 * @generated
		 */
		EClass BOOKING = eINSTANCE.getBooking();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.AdminImpl <em>Admin</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.AdminImpl
		 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl.ComponentsElementsPackageImpl#getAdmin()
		 * @generated
		 */
		EClass ADMIN = eINSTANCE.getAdmin();

	}

} //ComponentsElementsPackage
