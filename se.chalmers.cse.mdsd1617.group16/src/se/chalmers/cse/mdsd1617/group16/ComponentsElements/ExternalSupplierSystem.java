/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Supplier System</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage#getExternalSupplierSystem()
 * @model
 * @generated
 */
public interface ExternalSupplierSystem extends EObject {
} // ExternalSupplierSystem
