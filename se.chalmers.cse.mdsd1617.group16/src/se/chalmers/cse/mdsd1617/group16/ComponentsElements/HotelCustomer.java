/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelCustomerProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Customer</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage#getHotelCustomer()
 * @model
 * @generated
 */
public interface HotelCustomer extends IHotelCustomerProvides {
} // HotelCustomer
