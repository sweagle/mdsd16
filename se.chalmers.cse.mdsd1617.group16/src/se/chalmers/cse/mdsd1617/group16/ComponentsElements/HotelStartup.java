/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Startup</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage#getHotelStartup()
 * @model
 * @generated
 */
public interface HotelStartup extends IHotelStartupProvides {
} // HotelStartup
