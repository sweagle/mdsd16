/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Receptionist</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage#getReceptionist()
 * @model
 * @generated
 */
public interface Receptionist extends EObject {
} // Receptionist
