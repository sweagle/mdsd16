/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListFreeRooms;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ListRooms;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage#getRoomManager()
 * @model
 * @generated
 */
public interface RoomManager extends ListRooms, AddClearManager, ManageRoomManager, ListFreeRooms {
} // RoomManager
