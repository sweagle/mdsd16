/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClearBooking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.CoreBooking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.Booking;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BookingImpl extends MinimalEObjectImpl.Container implements Booking {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComponentsElementsPackage.Literals.BOOKING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking> listBookings() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean changeRoomType(int bookingId, String fromRoomType, String toRoomType) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean addRoomToBooking(int bookingId, String roomType) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean confirmBooking(int bookingID) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking getBookingFromRoom(int roomNumber) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking getBookingFromName(String firstName, String lastName) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean removeRoomFromBooking(int bookingId, String roomType) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean changeDate(int bookingId, String newStartDate, String newEndDate) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking> listCheckIns(String startDate, String endDate) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking> listCheckOut(String startDate, String endDate) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean checkIn(int bookingId) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addExtraCost(int bookingId, int roomNumber, String description, double extraPrice) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void cancelBooking(int bookingId) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoomReservation> getBookingRoomReservations(se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking booking) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking getBooking(int bookingId) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int createBooking(String firstName, String lastName, String startDate, String endDate) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double checkOutBooking(int bookingId) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int checkInRoom(String roomType, int bookingId) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double checkOutRoom(int roomId, int bookingId) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void clearBookings() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == CoreBooking.class) {
			switch (baseOperationID) {
				case ClassDiagramComponentsPackage.CORE_BOOKING___CREATE_BOOKING__STRING_STRING_STRING_STRING: return ComponentsElementsPackage.BOOKING___CREATE_BOOKING__STRING_STRING_STRING_STRING;
				case ClassDiagramComponentsPackage.CORE_BOOKING___CHECK_OUT_BOOKING__INT: return ComponentsElementsPackage.BOOKING___CHECK_OUT_BOOKING__INT;
				case ClassDiagramComponentsPackage.CORE_BOOKING___CHECK_IN_ROOM__STRING_INT: return ComponentsElementsPackage.BOOKING___CHECK_IN_ROOM__STRING_INT;
				case ClassDiagramComponentsPackage.CORE_BOOKING___CHECK_OUT_ROOM__INT_INT: return ComponentsElementsPackage.BOOKING___CHECK_OUT_ROOM__INT_INT;
				case ClassDiagramComponentsPackage.CORE_BOOKING___ADD_ROOM_TO_BOOKING__INT_STRING: return ComponentsElementsPackage.BOOKING___ADD_ROOM_TO_BOOKING__INT_STRING;
				case ClassDiagramComponentsPackage.CORE_BOOKING___CONFIRM_BOOKING__INT: return ComponentsElementsPackage.BOOKING___CONFIRM_BOOKING__INT;
				case ClassDiagramComponentsPackage.CORE_BOOKING___GET_BOOKING_FROM_ROOM__INT: return ComponentsElementsPackage.BOOKING___GET_BOOKING_FROM_ROOM__INT;
				case ClassDiagramComponentsPackage.CORE_BOOKING___GET_BOOKING_FROM_NAME__STRING_STRING: return ComponentsElementsPackage.BOOKING___GET_BOOKING_FROM_NAME__STRING_STRING;
				default: return -1;
			}
		}
		if (baseClass == ClearBooking.class) {
			switch (baseOperationID) {
				case ClassDiagramComponentsPackage.CLEAR_BOOKING___CLEAR_BOOKINGS: return ComponentsElementsPackage.BOOKING___CLEAR_BOOKINGS;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ComponentsElementsPackage.BOOKING___LIST_BOOKINGS:
				return listBookings();
			case ComponentsElementsPackage.BOOKING___CHANGE_ROOM_TYPE__INT_STRING_STRING:
				return changeRoomType((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case ComponentsElementsPackage.BOOKING___REMOVE_ROOM_FROM_BOOKING__INT_STRING:
				return removeRoomFromBooking((Integer)arguments.get(0), (String)arguments.get(1));
			case ComponentsElementsPackage.BOOKING___CHANGE_DATE__INT_STRING_STRING:
				return changeDate((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case ComponentsElementsPackage.BOOKING___LIST_CHECK_INS__STRING_STRING:
				return listCheckIns((String)arguments.get(0), (String)arguments.get(1));
			case ComponentsElementsPackage.BOOKING___LIST_CHECK_OUT__STRING_STRING:
				return listCheckOut((String)arguments.get(0), (String)arguments.get(1));
			case ComponentsElementsPackage.BOOKING___CHECK_IN__INT:
				return checkIn((Integer)arguments.get(0));
			case ComponentsElementsPackage.BOOKING___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE:
				addExtraCost((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
				return null;
			case ComponentsElementsPackage.BOOKING___CANCEL_BOOKING__INT:
				cancelBooking((Integer)arguments.get(0));
				return null;
			case ComponentsElementsPackage.BOOKING___GET_BOOKING_ROOM_RESERVATIONS__BOOKING:
				return getBookingRoomReservations((se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking)arguments.get(0));
			case ComponentsElementsPackage.BOOKING___GET_BOOKING__INT:
				return getBooking((Integer)arguments.get(0));
			case ComponentsElementsPackage.BOOKING___CREATE_BOOKING__STRING_STRING_STRING_STRING:
				return createBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case ComponentsElementsPackage.BOOKING___CHECK_OUT_BOOKING__INT:
				return checkOutBooking((Integer)arguments.get(0));
			case ComponentsElementsPackage.BOOKING___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case ComponentsElementsPackage.BOOKING___CHECK_OUT_ROOM__INT_INT:
				return checkOutRoom((Integer)arguments.get(0), (Integer)arguments.get(1));
			case ComponentsElementsPackage.BOOKING___ADD_ROOM_TO_BOOKING__INT_STRING:
				return addRoomToBooking((Integer)arguments.get(0), (String)arguments.get(1));
			case ComponentsElementsPackage.BOOKING___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case ComponentsElementsPackage.BOOKING___GET_BOOKING_FROM_ROOM__INT:
				return getBookingFromRoom((Integer)arguments.get(0));
			case ComponentsElementsPackage.BOOKING___GET_BOOKING_FROM_NAME__STRING_STRING:
				return getBookingFromName((String)arguments.get(0), (String)arguments.get(1));
			case ComponentsElementsPackage.BOOKING___CLEAR_BOOKINGS:
				clearBookings();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //BookingImpl
