/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import se.chalmers.cse.mdsd1617.group16.ComponentsElements.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ComponentsElementsFactoryImpl extends EFactoryImpl implements ComponentsElementsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ComponentsElementsFactory init() {
		try {
			ComponentsElementsFactory theComponentsElementsFactory = (ComponentsElementsFactory)EPackage.Registry.INSTANCE.getEFactory(ComponentsElementsPackage.eNS_URI);
			if (theComponentsElementsFactory != null) {
				return theComponentsElementsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ComponentsElementsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentsElementsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ComponentsElementsPackage.HOTEL_CUSTOMER: return createHotelCustomer();
			case ComponentsElementsPackage.EXTERNAL_SUPPLIER_SYSTEM: return createExternalSupplierSystem();
			case ComponentsElementsPackage.RECEPTIONIST: return createReceptionist();
			case ComponentsElementsPackage.ROOM_MANAGER: return createRoomManager();
			case ComponentsElementsPackage.HOTEL_STARTUP: return createHotelStartup();
			case ComponentsElementsPackage.BOOKING: return createBooking();
			case ComponentsElementsPackage.ADMIN: return createAdmin();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelCustomer createHotelCustomer() {
		HotelCustomerImpl hotelCustomer = new HotelCustomerImpl();
		return hotelCustomer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalSupplierSystem createExternalSupplierSystem() {
		ExternalSupplierSystemImpl externalSupplierSystem = new ExternalSupplierSystemImpl();
		return externalSupplierSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Receptionist createReceptionist() {
		ReceptionistImpl receptionist = new ReceptionistImpl();
		return receptionist;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomManager createRoomManager() {
		RoomManagerImpl roomManager = new RoomManagerImpl();
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartup createHotelStartup() {
		HotelStartupImpl hotelStartup = new HotelStartupImpl();
		return hotelStartup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Booking createBooking() {
		BookingImpl booking = new BookingImpl();
		return booking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Admin createAdmin() {
		AdminImpl admin = new AdminImpl();
		return admin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentsElementsPackage getComponentsElementsPackage() {
		return (ComponentsElementsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ComponentsElementsPackage getPackage() {
		return ComponentsElementsPackage.eINSTANCE;
	}

} //ComponentsElementsFactoryImpl
