/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ClassDiagramComponentsPackageImpl;

import se.chalmers.cse.mdsd1617.group16.ComponentsElements.Admin;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.Booking;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsFactory;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.ExternalSupplierSystem;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.HotelCustomer;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.HotelStartup;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.Receptionist;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.RoomManager;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ComponentsElementsPackageImpl extends EPackageImpl implements ComponentsElementsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hotelCustomerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalSupplierSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass receptionistEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomManagerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hotelStartupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass adminEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ComponentsElementsPackageImpl() {
		super(eNS_URI, ComponentsElementsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ComponentsElementsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ComponentsElementsPackage init() {
		if (isInited) return (ComponentsElementsPackage)EPackage.Registry.INSTANCE.getEPackage(ComponentsElementsPackage.eNS_URI);

		// Obtain or create and register package
		ComponentsElementsPackageImpl theComponentsElementsPackage = (ComponentsElementsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ComponentsElementsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ComponentsElementsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		ClassDiagramComponentsPackageImpl theClassDiagramComponentsPackage = (ClassDiagramComponentsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ClassDiagramComponentsPackage.eNS_URI) instanceof ClassDiagramComponentsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ClassDiagramComponentsPackage.eNS_URI) : ClassDiagramComponentsPackage.eINSTANCE);

		// Create package meta-data objects
		theComponentsElementsPackage.createPackageContents();
		theClassDiagramComponentsPackage.createPackageContents();

		// Initialize created meta-data
		theComponentsElementsPackage.initializePackageContents();
		theClassDiagramComponentsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theComponentsElementsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ComponentsElementsPackage.eNS_URI, theComponentsElementsPackage);
		return theComponentsElementsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHotelCustomer() {
		return hotelCustomerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExternalSupplierSystem() {
		return externalSupplierSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReceptionist() {
		return receptionistEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomManager() {
		return roomManagerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHotelStartup() {
		return hotelStartupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooking() {
		return bookingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdmin() {
		return adminEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentsElementsFactory getComponentsElementsFactory() {
		return (ComponentsElementsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		hotelCustomerEClass = createEClass(HOTEL_CUSTOMER);

		externalSupplierSystemEClass = createEClass(EXTERNAL_SUPPLIER_SYSTEM);

		receptionistEClass = createEClass(RECEPTIONIST);

		roomManagerEClass = createEClass(ROOM_MANAGER);

		hotelStartupEClass = createEClass(HOTEL_STARTUP);

		bookingEClass = createEClass(BOOKING);

		adminEClass = createEClass(ADMIN);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ClassDiagramComponentsPackage theClassDiagramComponentsPackage = (ClassDiagramComponentsPackage)EPackage.Registry.INSTANCE.getEPackage(ClassDiagramComponentsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		hotelCustomerEClass.getESuperTypes().add(theClassDiagramComponentsPackage.getIHotelCustomerProvides());
		roomManagerEClass.getESuperTypes().add(theClassDiagramComponentsPackage.getListRooms());
		roomManagerEClass.getESuperTypes().add(theClassDiagramComponentsPackage.getAddClearManager());
		roomManagerEClass.getESuperTypes().add(theClassDiagramComponentsPackage.getManageRoomManager());
		roomManagerEClass.getESuperTypes().add(theClassDiagramComponentsPackage.getListFreeRooms());
		hotelStartupEClass.getESuperTypes().add(theClassDiagramComponentsPackage.getIHotelStartupProvides());
		bookingEClass.getESuperTypes().add(theClassDiagramComponentsPackage.getManageBooking());
		bookingEClass.getESuperTypes().add(theClassDiagramComponentsPackage.getCoreBooking());
		bookingEClass.getESuperTypes().add(theClassDiagramComponentsPackage.getClearBooking());

		// Initialize classes, features, and operations; add parameters
		initEClass(hotelCustomerEClass, HotelCustomer.class, "HotelCustomer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(externalSupplierSystemEClass, ExternalSupplierSystem.class, "ExternalSupplierSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(receptionistEClass, Receptionist.class, "Receptionist", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(roomManagerEClass, RoomManager.class, "RoomManager", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(hotelStartupEClass, HotelStartup.class, "HotelStartup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bookingEClass, Booking.class, "Booking", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(adminEClass, Admin.class, "Admin", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //ComponentsElementsPackageImpl
