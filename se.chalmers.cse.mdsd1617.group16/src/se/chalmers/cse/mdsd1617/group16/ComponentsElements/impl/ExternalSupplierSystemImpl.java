/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.ExternalSupplierSystem;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>External Supplier System</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExternalSupplierSystemImpl extends MinimalEObjectImpl.Container implements ExternalSupplierSystem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExternalSupplierSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComponentsElementsPackage.Literals.EXTERNAL_SUPPLIER_SYSTEM;
	}

} //ExternalSupplierSystemImpl
