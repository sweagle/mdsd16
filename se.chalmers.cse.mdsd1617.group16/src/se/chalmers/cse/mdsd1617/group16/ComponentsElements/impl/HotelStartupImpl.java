/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.HotelStartup;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Startup</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class HotelStartupImpl extends MinimalEObjectImpl.Container implements HotelStartup {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HotelStartupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComponentsElementsPackage.Literals.HOTEL_STARTUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void startup(int numRooms) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ComponentsElementsPackage.HOTEL_STARTUP___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelStartupImpl
