/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.Receptionist;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Receptionist</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ReceptionistImpl extends MinimalEObjectImpl.Container implements Receptionist {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReceptionistImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComponentsElementsPackage.Literals.RECEPTIONIST;
	}

} //ReceptionistImpl
