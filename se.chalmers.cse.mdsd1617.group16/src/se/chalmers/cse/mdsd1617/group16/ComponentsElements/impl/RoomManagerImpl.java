/**
 */
package se.chalmers.cse.mdsd1617.group16.ComponentsElements.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.AddClearManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ClassDiagramComponentsPackage;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.ManageRoomManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomTypeImpl;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.ComponentsElementsPackage;
import se.chalmers.cse.mdsd1617.group16.ComponentsElements.RoomManager;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RoomManagerImpl extends MinimalEObjectImpl.Container implements RoomManager {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComponentsElementsPackage.Literals.ROOM_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FreeRoomTypesDTO> listFreeRooms(String startDate, String endDate, int minBeds) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> listRooms() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<Integer, Room> listOccupiedRooms(String date) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public RoomType addRoomType(String name, double price, int numOfBeds, String additionalFeatures) {
		
		RoomType rt = new RoomTypeImpl(name, price, numOfBeds, additionalFeatures);
		
		
		return rt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room addRoom(int roomNumber, RoomType roomType) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void clearRooms() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void clearRoomTypes() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void changeRoomType(int roomNumber, RoomType roomType) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeRoom(int roomNumber) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void blockRoom(int roomNumber) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unblockRoom(int roomNumber) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void updateRoomType(String name, double price, int numberOfBeds, String additionalFeatures) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean removeRoomType(String name) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomType getRoomType(String roomTypeDescription) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == AddClearManager.class) {
			switch (baseOperationID) {
				case ClassDiagramComponentsPackage.ADD_CLEAR_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING: return ComponentsElementsPackage.ROOM_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING;
				case ClassDiagramComponentsPackage.ADD_CLEAR_MANAGER___ADD_ROOM__INT_ROOMTYPE: return ComponentsElementsPackage.ROOM_MANAGER___ADD_ROOM__INT_ROOMTYPE;
				case ClassDiagramComponentsPackage.ADD_CLEAR_MANAGER___CLEAR_ROOMS: return ComponentsElementsPackage.ROOM_MANAGER___CLEAR_ROOMS;
				case ClassDiagramComponentsPackage.ADD_CLEAR_MANAGER___CLEAR_ROOM_TYPES: return ComponentsElementsPackage.ROOM_MANAGER___CLEAR_ROOM_TYPES;
				default: return -1;
			}
		}
		if (baseClass == ManageRoomManager.class) {
			switch (baseOperationID) {
				case ClassDiagramComponentsPackage.MANAGE_ROOM_MANAGER___CHANGE_ROOM_TYPE__INT_ROOMTYPE: return ComponentsElementsPackage.ROOM_MANAGER___CHANGE_ROOM_TYPE__INT_ROOMTYPE;
				case ClassDiagramComponentsPackage.MANAGE_ROOM_MANAGER___REMOVE_ROOM__INT: return ComponentsElementsPackage.ROOM_MANAGER___REMOVE_ROOM__INT;
				case ClassDiagramComponentsPackage.MANAGE_ROOM_MANAGER___BLOCK_ROOM__INT: return ComponentsElementsPackage.ROOM_MANAGER___BLOCK_ROOM__INT;
				case ClassDiagramComponentsPackage.MANAGE_ROOM_MANAGER___UNBLOCK_ROOM__INT: return ComponentsElementsPackage.ROOM_MANAGER___UNBLOCK_ROOM__INT;
				case ClassDiagramComponentsPackage.MANAGE_ROOM_MANAGER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING: return ComponentsElementsPackage.ROOM_MANAGER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING;
				case ClassDiagramComponentsPackage.MANAGE_ROOM_MANAGER___REMOVE_ROOM_TYPE__STRING: return ComponentsElementsPackage.ROOM_MANAGER___REMOVE_ROOM_TYPE__STRING;
				case ClassDiagramComponentsPackage.MANAGE_ROOM_MANAGER___GET_ROOM_TYPE__STRING: return ComponentsElementsPackage.ROOM_MANAGER___GET_ROOM_TYPE__STRING;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ComponentsElementsPackage.ROOM_MANAGER___LIST_FREE_ROOMS__STRING_STRING_INT:
				return listFreeRooms((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2));
			case ComponentsElementsPackage.ROOM_MANAGER___LIST_ROOMS:
				return listRooms();
			case ComponentsElementsPackage.ROOM_MANAGER___LIST_OCCUPIED_ROOMS__STRING:
				return listOccupiedRooms((String)arguments.get(0));
			case ComponentsElementsPackage.ROOM_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				return addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
			case ComponentsElementsPackage.ROOM_MANAGER___ADD_ROOM__INT_ROOMTYPE:
				return addRoom((Integer)arguments.get(0), (RoomType)arguments.get(1));
			case ComponentsElementsPackage.ROOM_MANAGER___CLEAR_ROOMS:
				clearRooms();
				return null;
			case ComponentsElementsPackage.ROOM_MANAGER___CLEAR_ROOM_TYPES:
				clearRoomTypes();
				return null;
			case ComponentsElementsPackage.ROOM_MANAGER___CHANGE_ROOM_TYPE__INT_ROOMTYPE:
				changeRoomType((Integer)arguments.get(0), (RoomType)arguments.get(1));
				return null;
			case ComponentsElementsPackage.ROOM_MANAGER___REMOVE_ROOM__INT:
				removeRoom((Integer)arguments.get(0));
				return null;
			case ComponentsElementsPackage.ROOM_MANAGER___BLOCK_ROOM__INT:
				blockRoom((Integer)arguments.get(0));
				return null;
			case ComponentsElementsPackage.ROOM_MANAGER___UNBLOCK_ROOM__INT:
				unblockRoom((Integer)arguments.get(0));
				return null;
			case ComponentsElementsPackage.ROOM_MANAGER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				updateRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
				return null;
			case ComponentsElementsPackage.ROOM_MANAGER___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case ComponentsElementsPackage.ROOM_MANAGER___GET_ROOM_TYPE__STRING:
				return getRoomType((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomManagerImpl
