package test;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.EList;
import org.junit.Before;
import org.junit.Test;


import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.BookingManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.AdminImpl;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingManagerImpl;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.HotelStartupProvidesImpl;

import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomManagerImpl;

public class TestAdmin {
	
	AdminImpl admin;
	RoomManager rManager;
	private final int NUM_ROOMS = 20;
	
	// UC 2.2.9
    @Before public void initialize() {
    	rManager = RoomManagerImpl.getRoomManagerImpl();
        
    	BookingManager bManager = BookingManagerImpl.getBookingManagerImpl();
    	IHotelStartupProvides hStartup = new HotelStartupProvidesImpl();

    	admin = new AdminImpl(rManager, hStartup);
    	admin.startup(NUM_ROOMS);
    	
    	
    	// check correct # of rooms created
    	System.out.println(rManager.getRooms().size());
    	assertEquals(20, rManager.getRooms().size(), 0);
    	
    	// check that there are no bookings
    	assertEquals(0, bManager.getBookings().size(), 0);
    	
    }

    //UC. 2.2.1
    
	@Test
	public void addRoomTypeTest() {
		// check here if it even exists correctly
		EList<RoomType> before = rManager.getRoomTypes();
		assertEquals(1, before.size(), 0);

		admin.addRoomType("Single", 100, 1, "");

		EList<RoomType> after = rManager.getRoomTypes();
		assertEquals(2, after.size(), 0);
	}
	
	// UC 2.2.4
	@Test
	public void addRoomTest() {
		// check here if it even exists correctly
		EList<Room> before = rManager.getRooms();
		assertEquals(20, before.size(), 0);

		admin.addRoom(20, rManager.getRoomType("Double"));

		EList<Room> after = rManager.getRooms();
		assertEquals(21, after.size(), 0);
	}
	
	// UC 2.2.2
	@Test
	public void updateRoomTypeTest() {
		//check here if it even exists correctly
		RoomType before = rManager.getRoomType("Double");
		assertEquals(850, before.getCost(), 1);
		assertEquals(2, before.getNumberOfBeds(), 1);
		assertEquals("", before.getAdditionalFeatures());
		
		admin.updateRoomType("Double", 800, 3, "");
		
		//assert
		RoomType after = rManager.getRoomType("Double");
		assertEquals(800, after.getCost(), 1);
		assertEquals(3, after.getNumberOfBeds(), 1);
		assertEquals("", after.getAdditionalFeatures());
	}

	
	// UC 2.2.3
	@Test
	public void removeRoomTypeTest() {
		//check here if it even exists correctly
		
		admin.addRoomType("test", 0, 5, "");
		
		EList<RoomType> before = rManager.getRoomTypes();
		
		assertEquals(2, before.size(), 0);
		
		assertFalse(admin.removeRoomType("Double")); // Should not be able to remove since rooms exists with this type. Return False
		
		assertEquals(2, before.size(), 0); // Size of the list should be the same
		
		assertTrue(admin.removeRoomType("test")); // Should be able to remove this since no rooms has this roomtype
		
		assertEquals(1, before.size(), 0); // Size should now be 1 
		
	}
	
	/*
	 * Tests UC 2.2.5
	 */
	@Test
	public void changeRoomTypeTest() {
		//check here if it even exists correctly
		Room before = rManager.getRooms().get(5);
		assertEquals(5, before.getRoomNumber(), 0);
		assertEquals(rManager.getRoomType("Double"), before.getType());
		
		admin.addRoomType("King", 250, 1, "");
		admin.changeRoomType(5, rManager.getRoomType("King"));
		

		Room after = rManager.getRooms().get(5);
		assertEquals(5, after.getRoomNumber(), 0);
		assertEquals(rManager.getRoomType("King"), after.getType());
		
	}
	
	@Test
	public void removeRoomTest() {
		// check here if it even exists correctly
		EList<Room> before = rManager.getRooms();
		assertEquals(20, before.size(), 0);

		admin.removeRoom(0);

		assertEquals(19, before.size(), 0);
		
		admin.removeRoom(10);

		assertEquals(18, before.size(), 0);
		
		admin.removeRoom(19);

		assertEquals(17, before.size(), 0);
	}
	
	//UC 2.2.8, UC 2.2.7
	
	@Test
	public void blockingRoomTest() {
		// check here if it even exists correctly
		EList<Room> roomList = rManager.getRooms();
		Room blockedRoom = null;
		for (Room room : roomList) {
			if (room.getRoomNumber() == 5) {
					blockedRoom = room;
			}
		}
		assertEquals(false, blockedRoom.isBlocked());
		
		admin.blockRoom(5);
		
		assertEquals(true, blockedRoom.isBlocked());
		
		admin.unblockRoom(5);
		
		assertEquals(false, blockedRoom.isBlocked());
	}
	
}
