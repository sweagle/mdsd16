package test;

import static org.junit.Assert.*;


import java.util.Map;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.junit.Before;
import org.junit.Test;


import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Admin;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Booking;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.BookingManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.IHotelStartupProvides;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Receptionist;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.Room;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomManager;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomReservation;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.RoomType;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.AdminImpl;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.BookingManagerImpl;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.HotelStartupProvidesImpl;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.ReceptionistImpl;
import se.chalmers.cse.mdsd1617.group16.ClassDiagramComponents.impl.RoomManagerImpl;

public class TestReceptionist {
	Receptionist receptionist;
	BookingManager bManager;
	RoomManager rManager;
	private final int NUM_ROOMS = 20;
	private final int NUM_OF_ROOMS_TO_BOOK = 3;
	
	
	// We have only created this customer in the bankning component. Always using this when testing payment
	private String cardHolderFirstName = "TestFirst1";
	private String cardHolderLastName = "TestLast";
	private String cardHolderCCNumber = "00000003";
	private String cardHolderCCV = "000";
	private int cardHolderExMonth = 11; 
	private int cardHolderExYear = 16;
	
	

    @Before public void initialize() {
    	rManager = RoomManagerImpl.getRoomManagerImpl();
        bManager = BookingManagerImpl.getBookingManagerImpl();
    	IHotelStartupProvides hStartup = new HotelStartupProvidesImpl();
    	receptionist = new ReceptionistImpl();
    	Admin admin = new AdminImpl(rManager, hStartup);
    	
    	admin.startup(NUM_ROOMS);


    }

    //UC 2.1.9
	@Test
	public void testListCheckInsForSpecificDay(){
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		df.setLenient(false);
		Date e = new Date();
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(e); 
		c.add(Calendar.DATE, 5);
		Date tomorrow = c.getTime();
		
		
		String startDate = df.format(e);
		String endDate = df.format(tomorrow);
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		System.out.println(bookingId);
		
		receptionist.addRoomToBooking("Double", bookingId);
		receptionist.confirmBooking(bookingId);
		receptionist.checkIn(bookingId);
		
		EList<Booking> checkIns = receptionist.listCheckIns(startDate, endDate);
		
		System.out.println("startdate: " + startDate + " enddate: " + endDate + " length: " + checkIns.size());
		assertTrue(checkIns.size() == 1);
	}
	
	// UC 2.1.10
	@Test
	public void testListCheckOutsForSpecificDay() {
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		df.setLenient(false);
		Date e = new Date();

		Calendar c = Calendar.getInstance();
		c.setTime(e);
		c.add(Calendar.DATE, 5);
		Date tomorrow = c.getTime();

		String startDate = df.format(e);
		String endDate = df.format(tomorrow);
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		System.out.println(bookingId);

		receptionist.addRoomToBooking("Double", bookingId);
		receptionist.confirmBooking(bookingId);
		receptionist.checkIn(bookingId);
		int roomNumber = receptionist.checkInRoom("Double", bookingId);
		receptionist.initiateCheckout(bookingId);
		receptionist.initiateRoomCheckout(roomNumber, bookingId);
		
		receptionist.payRoomDuringCheckout(0, "00000000", "000", 11, 16, firstName, lastName);

		EList<Booking> checkOuts = receptionist.listCheckOut(startDate, endDate);

		System.out.println("startdate: " + startDate + " enddate: " + endDate + " length: " + checkOuts.size());
		assertTrue(checkOuts.size() == 1);
	}

	// UC 2.1.1
	@Test
	public void makeABooking(){
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		for(int i=0;i<NUM_OF_ROOMS_TO_BOOK;i++){
			if (!receptionist.addRoomToBooking(roomTypeDescription, bookingId)){
				fail("Couldn't add room to Booking");
			}
		}
		//Now make sure that the booking exists and is confirmed
		//receptionist.confirmBooking(bookingId) returns true if booking is successful
		if (!receptionist.confirmBooking(bookingId)) {
			fail("Booking was not succesfully confirmed.");
		}
		
	}
	
	// UC 2.1.6
	@Test
	public void cancelABooking(){
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		for(int i=0;i<NUM_OF_ROOMS_TO_BOOK;i++){
			if (!receptionist.addRoomToBooking(roomTypeDescription, bookingId)){
				fail("Couldn't add room to Booking");
			}
		}
		receptionist.confirmBooking(bookingId);
		
		EList <FreeRoomTypesDTO> frBefore = receptionist.getFreeRooms(2, startDate, endDate);
		
		receptionist.cancelBooking(bookingId); // Should remove all reservations and set booking as not confirmed 
		
		EList <FreeRoomTypesDTO> frAfter = receptionist.getFreeRooms(2, startDate, endDate);
		
		
		assertTrue(frAfter.get(0).getNumFreeRooms() == 20  && frBefore.get(0).getNumFreeRooms() == 17); 
		
	}

	/*
	 * (Tests UC 2.1.1, ensuring that it isn't possible to make a 
	 * booking with an invalid date by checking non-existing dates,
	 * incorrectly ordered start- and end dates, and the start- and
	 * end dates being the same.)
	 */
	@Test
	public void makeABookingWithInvalidDate(){
		// Testing with enddate before startdate
		String startDate = "20170829";
		String endDate = "20170826";
		String firstName = "First name";
		String lastName = "Last name";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		if (bookingId != -1) fail("end before start: bookingId should return -1");
		

		// Testing with date that does not exists
		String startDate1 = "20170832";
		String endDate1 = "20170826";
		int bookingId1 = receptionist.initiateBooking(firstName, startDate1, endDate1, lastName);

		if (bookingId1 != -1) fail("startdate invalid: bookingId should return -1");

		

		// Testing with date on the same day
		String startDate2 = "20170826";
		String endDate2 = "20170826";
		int bookingId2 = receptionist.initiateBooking(firstName, startDate2, endDate2, lastName);

		if (bookingId2 != -1) fail("start end on sameday: bookingId should return -1");
		
		// Testing with date before current date
		String startDate3 = "20150824";
		String endDate3 = "20150826";
		int bookingId3 = receptionist.initiateBooking(firstName, startDate3, endDate3, lastName);


		if (bookingId3 != -1) fail("dates after todays date: bookingId should return -1");


	}
	
	
	/*
	 * Tests UC 2.1.8
	 */
	@Test
	public void testListOccupiedRooms() {
		
		String date = "20170827";
		
		String startDate = "20170820";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		receptionist.addRoomToBooking(roomTypeDescription, bookingId);
		receptionist.confirmBooking(bookingId);
		receptionist.checkIn(bookingId);
		receptionist.checkInRoom(roomTypeDescription, bookingId);
		
		String startDate2 = "20170826";
		String endDate2 = "20170830";
		String firstName2 = "TestFirst2";
		String lastName2 = "TestLast2";
		int bookingId2 = receptionist.initiateBooking(firstName2, startDate2, endDate2, lastName2);
		receptionist.addRoomToBooking(roomTypeDescription, bookingId2);
		receptionist.confirmBooking(bookingId2);
		receptionist.checkIn(bookingId2);
		receptionist.checkInRoom(roomTypeDescription, bookingId2);
		
		String startDate3 = "20170822";
		String endDate3 = "20170828";
		String firstName3 = "TestFirst3";
		String lastName3 = "TestLast3";
		int bookingId3 = receptionist.initiateBooking(firstName3, startDate3, endDate3, lastName3);
		receptionist.addRoomToBooking(roomTypeDescription, bookingId3);
		receptionist.confirmBooking(bookingId3);
		receptionist.checkIn(bookingId3);
		receptionist.checkInRoom(roomTypeDescription, bookingId3);
		
		
		Map<Integer, Room> occupiedRooms = bManager.listOccupiedRooms(date);
		
		
		if (occupiedRooms.size() != 3) {
			fail("Incorrect number of occupied rooms.");
		}
		
		EList<Integer> bookingIds = new BasicEList<Integer>();
		
		bookingIds.add(bookingId);
		bookingIds.add(bookingId2);
		bookingIds.add(bookingId3);
		
		for (Integer id : bookingIds) {
	
			if (!occupiedRooms.containsKey(id)){
				fail("Proper booking ID is not contained.");
			}
			if(!occupiedRooms.get(id).getType().getType().equals(roomTypeDescription)){
				fail("Wrong type of occupied room");
			}
		}		
		
	}


	/*
	 * Tests use case 2.1.3. Makes sure a room is marked as occupied and linked to a specified booking when checked in.
	 */
	@Test
	public void testCheckInBooking(){
		//Simulate an existing booking
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		receptionist.addRoomToBooking(roomTypeDescription, bookingId);
		receptionist.confirmBooking(bookingId);
		Booking booking = receptionist.getBooking(bookingId);
		
		//Check in
		assertEquals(0,receptionist.checkInRoom(roomTypeDescription, bookingId));
		assertTrue(receptionist.checkIn(bookingId));
		
		//Make sure a room of the correct type is linked to the booking
		RoomReservation rr = booking.getReservations().get(0);
		assertEquals(rr.getRoomType(),rManager.getRoomType(roomTypeDescription));
		assertEquals(rr.getBooking(),booking);
		
		//Make sure the booking has been checked in
		assertTrue(booking.isCheckedIn());
				
		//Make sure the room is marked as occupied
		assertFalse(rManager.getRooms().get(rr.getRoomId()).isFree());		
	}
	
	@Test
	public void searchForFreeRooms(){
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		for(int i=0;i<NUM_OF_ROOMS_TO_BOOK;i++){
			if (!receptionist.addRoomToBooking(roomTypeDescription, bookingId)){
				fail("Couldn't add room to Booking");
			}
		}
		receptionist.confirmBooking(bookingId);
		//Now make sure that the booking exists and is confirmed

		EList <FreeRoomTypesDTO> fr = receptionist.getFreeRooms(2, startDate, endDate);

		System.out.println("RoomTypes: " + fr.size());
		System.out.println("Rooms: " + fr.get(0).getNumFreeRooms());

	}

	/*
	 * Tests UC 2.1.13
	 */
	@Test
	public void addExtraCostToRoom(){
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();

		receptionist.addRoomToBooking(roomTypeDescription, bookingId);
		receptionist.confirmBooking(bookingId);

		Booking booking = bManager.getBooking(bookingId);
		RoomReservation reservation = bManager.getBookingRoomReservations(booking).get(0);

		double normalCost = reservation.getRoomType().getCost();
		bManager.addExtraCost(bookingId, reservation.getRoomId(), "Laundry service", 100);

		assertTrue(normalCost+100==reservation.getTotalCost());
	}

	@Test
	public void confirmEmptyBooking() {
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		receptionist.confirmBooking(bookingId);

		assertEquals(false, bManager.getBooking(bookingId).isConfirmed());
	}
	
	
	@Test
	public void testBookingAddRoomWithInvalidId(){
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		for(int i=0;i<NUM_OF_ROOMS_TO_BOOK;i++){
			if (receptionist.addRoomToBooking(roomTypeDescription, bookingId+1)){
				fail("Could add room to Booking with invalid id");
			}
		}
		receptionist.confirmBooking(bookingId);
	}

	@Test
	public void testBookingConfirmWithInvalidID(){
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();


		receptionist.addRoomToBooking(roomTypeDescription, bookingId);
		assertTrue(receptionist.confirmBooking(bookingId*-1) == false);
	}



	@Test
	public void testBookingCheckInWrongId(){
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		for(int i=0;i<NUM_OF_ROOMS_TO_BOOK;i++){
			if (!receptionist.addRoomToBooking(roomTypeDescription, bookingId)){
				fail("Couldn't add room to Booking");
			}
		}
		receptionist.confirmBooking(bookingId);
		for(int i=0;i<NUM_OF_ROOMS_TO_BOOK;i++){
			int roomNumber = receptionist.checkInRoom(roomTypeDescription, bookingId+1);
			assertTrue(roomNumber==-1);
		}
		assertFalse(receptionist.checkIn(bookingId+1));
	}

	//One room shall only be checked in once. UC 2.1.12, UC 2.1.11
	@Test
	public void testBookingCheckInOutIndividualSuccess(){
		
		System.out.println("Testing that one room is only checked in once");
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = this.cardHolderFirstName;
		String lastName = this.cardHolderLastName;
		
		double bal = 0;
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		
		// Add NUM_OF_ROOMS_TO_BOOK in o the booking
		for(int i=0;i<NUM_OF_ROOMS_TO_BOOK;i++){
			receptionist.addRoomToBooking(roomTypeDescription, bookingId);
		}
		
		receptionist.confirmBooking(bookingId);
		boolean[] checkedIn = new boolean[NUM_ROOMS];
		int roomNumber = -1;
		int [] rn = new int [3];
		
		// Check in each room individually in the booking
		for(int i=0;i<NUM_OF_ROOMS_TO_BOOK;i++){
			roomNumber = receptionist.checkInRoom(roomTypeDescription, bookingId);
			rn[i] = roomNumber;
			if (checkedIn[roomNumber]) fail("One room shall only be checked in once.");
			checkedIn[roomNumber] = true;
		}
		
		// Then add a card (might already exists), and then fill that card with 3000 kr.
		try {
			se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires bankingAdmin = se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires.instance();
	          
	          // Add new credit card
			if (bankingAdmin.addCreditCard(this.cardHolderCCNumber, this.cardHolderCCV, this.cardHolderExMonth, this.cardHolderExYear, firstName, lastName)) {
				System.out.println("Successfully added credit card.");
			} else {
				System.out.println("Error while adding credit card. (Might already exist)");
			}
	          
	        bal = bankingAdmin.makeDeposit(this.cardHolderCCNumber, this.cardHolderCCV, this.cardHolderExMonth, this.cardHolderExYear, firstName, lastName, 3000);
	         
	        if (bal == -1 ) System.out.println("Could not makeDeposit");
	          
	    }catch (SOAPException e){
	         e.printStackTrace();
	    }
		
		// Checkout first room
		receptionist.initiateRoomCheckout(rn[0], bookingId);
		//System.out.println("return of checkout room 0" + receptionist.initiateRoomCheckout(0, bookingId));
		assertTrue(receptionist.payRoomDuringCheckout(0, this.cardHolderCCNumber, this.cardHolderCCV, this.cardHolderExMonth, this.cardHolderExYear, firstName, lastName));
		assertFalse(bManager.getBooking(bookingId).isPayed());
		
		// Checkout second room
		receptionist.initiateRoomCheckout(rn[1], bookingId);
		assertTrue(receptionist.payRoomDuringCheckout(1, this.cardHolderCCNumber, this.cardHolderCCV, this.cardHolderExMonth, this.cardHolderExYear, firstName, lastName));
		assertFalse(bManager.getBooking(bookingId).isPayed());
		
		//Checkout third room. It should now be payed and done
		receptionist.initiateRoomCheckout(rn[2], bookingId);
		assertTrue(receptionist.payRoomDuringCheckout(2, this.cardHolderCCNumber, this.cardHolderCCV, this.cardHolderExMonth, this.cardHolderExYear, firstName, lastName));
		assertTrue(bManager.getBooking(bookingId).isPayed());
		
		assertTrue(bManager.getBooking(bookingId).isCheckedOut());
		
		
		
		
	}
	

	@Test
	public void testMultiBookTooMany(){
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		for(int i=0;i<NUM_ROOMS;i++){
			if (!receptionist.addRoomToBooking(roomTypeDescription, bookingId)){
				fail("Couldn't add room to Booking");
			}
		}
		int bookingId2 = receptionist.initiateBooking(firstName+"2", startDate, endDate, lastName+"2");
		
		if(receptionist.addRoomToBooking(roomTypeDescription, bookingId2)){
			fail("Could add room to booking when no rooms where available");
		}
		
		receptionist.confirmBooking(bookingId);
		
		
		
		//Now make sure that the booking exists and is confirmed


	}
	
	@Test
	public void bookingAddRoomWithoutFreeRooms(){
		String startDate = "20170801";
		String endDate = "20170803";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		
		//Book all the rooms of a type
		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		for(int i=0; i < NUM_ROOMS; i++) {
			receptionist.addRoomToBooking(roomTypeDescription, bookingId);
		}
		receptionist.confirmBooking(bookingId);
		
		//Now try to book more rooms of the same type
		int bookingId2 = receptionist.initiateBooking(firstName+"2", startDate, endDate, lastName+"2");
		if(receptionist.addRoomToBooking(roomTypeDescription, bookingId2)){
			fail("Not enough free rooms of this type");
		}
		
		receptionist.confirmBooking(bookingId2);
	}
	
	
	
	

	@Test
	public void testBookingPayWithWrongInfoIndividual() throws SOAPException{
		String startDate = "20170801";
		String endDate = "20170803";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		
		//Book all the rooms of a type
		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		
		
		receptionist.addRoomToBooking(roomTypeDescription, bookingId);
		receptionist.confirmBooking(bookingId);
		int roomNr = receptionist.checkInRoom(roomTypeDescription, bookingId);
		
		assertFalse(receptionist.payRoomDuringCheckout(roomNr, "5545555555555555", "100", 10, 2020, firstName, lastName));
	}
	
	
	// UC 2.1.7
	@Test
	public void testListBookings(){
		String startDate = "20170801";
		String endDate = "20170803";
		String firstName = "TestFirst";
		String lastName = "TestLast";

		String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		EList<Booking> bookings = new BasicEList<Booking>();
		for(int i=0;i<10;i++){
			int bookingId = receptionist.initiateBooking(firstName + i, startDate, endDate, lastName + i);
			if (!receptionist.addRoomToBooking(roomTypeDescription, bookingId)){
				fail("Couldn't add room to Booking");
			}
			receptionist.confirmBooking(bookingId);
			bookings.add(bManager.getBooking(bookingId));
		}
		
		assertTrue(receptionist.listBookings().equals(bookings));
	}
	
	/*
	 * Tests UC 2.1.5. The receptionist should be able to edit room types, the period of time, and the number of rooms for a booking.
	 */
	@Test
	public void testEditBooking(){
		// Create a new booking
		String startDate = "20170826";
		String endDate = "20170829";
		String firstName = "TestFirst";
		String lastName = "TestLast";
		int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		String firstRoomTypeDesc = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
		
		// Make sure there are different room types in the system
		RoomType secondRoomType = rManager.addRoomType("Triple", 1095, 3, "");
		for(int i = 1; i < 5; i++) {
			rManager.addRoom(NUM_ROOMS+i, secondRoomType);
		}
		
		//Add a few rooms
		receptionist.addRoomToBooking(firstRoomTypeDesc, bookingId);
		receptionist.addRoomToBooking(firstRoomTypeDesc, bookingId);
		receptionist.addRoomToBooking(firstRoomTypeDesc, bookingId);
		
		//Change the room type
		Booking booking = receptionist.getBooking(bookingId);
		if(!receptionist.changeRoomType(bookingId, firstRoomTypeDesc, secondRoomType.getType()))
			fail("Couldn't change room type");
		
		for(RoomReservation rr : booking.getReservations()){
			assertEquals(secondRoomType,rr.getRoomType());
		}
		
		
		//Change the period of time
		String startDate1 = "20170827";
		String startDate2 = "20170825";
		String endDate1 = "20170830";
		String endDate2 = "20170828";
		if(!receptionist.changeDate(bookingId, startDate1, endDate))
			fail("Couldn't move the start date forward");
		assertEquals(startDate1, booking.getStartDate());
		if(!receptionist.changeDate(bookingId, startDate2, endDate))
			fail("Couldn't move the start date backward");
		assertEquals(startDate2, booking.getStartDate());
		if(!receptionist.changeDate(bookingId, startDate, endDate1))
			fail("Couldn't move the end date forward");
		assertEquals(endDate1, booking.getEndDate());
		if(!receptionist.changeDate(bookingId, startDate, endDate2))
			fail("Couldn't move the end date backward");
		assertEquals(endDate2, booking.getEndDate());
		
		//Ensure that the booking can't be updated with invalid dates
		if(receptionist.changeDate(bookingId, endDate, startDate))
			fail("It shouldn't be possible to have the start date after the end date");
		
		//Change the number of rooms
		int numberOfRooms = receptionist.getBooking(bookingId).getReservations().size();
		if(!receptionist.removeRoomFromBooking(bookingId, secondRoomType.getType()))
			fail("Couldn't remove room from booking");
		assertEquals(numberOfRooms-1,booking.getReservations().size());	
		if(!receptionist.addRoomToBooking(secondRoomType.getType(), bookingId))
			fail("Couldn't add room to booking");
		assertEquals(numberOfRooms,booking.getReservations().size());	
		
		// Confirm the booking
		if (!receptionist.confirmBooking(bookingId)) {
			fail("Booking was not succesfully confirmed");
		}
		
		
		
	}
	
	
	
	
	
    /* Tests use case 2.1.4. Makes sure that a room is checked out and paid considering all room prices and extras.*/
   @Test
   public void testCheckInOutBooking(){
       //Simulate an existing booking
       String startDate = "20170826";
       String endDate = "20170829";
       String firstName = this.cardHolderFirstName;
       String lastName = this.cardHolderLastName;
       
       int bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
       
       String roomTypeDescription = receptionist.getFreeRooms(0, startDate, endDate).get(0).getRoomTypeDescription();
       receptionist.addRoomToBooking(roomTypeDescription, bookingId);
       receptionist.confirmBooking(bookingId);
       Booking booking = receptionist.getBooking(bookingId);
       
       //Check in
       assertEquals(0,receptionist.checkInRoom(roomTypeDescription, bookingId));
       assertTrue(receptionist.checkIn(bookingId));
       
       //Make sure a room of the correct type is linked to the booking
       RoomReservation rr = booking.getReservations().get(0);
       assertEquals(rr.getRoomType(),rManager.getRoomType(roomTypeDescription));
       assertEquals(rr.getBooking(),booking);
       
       //Make sure the booking has been checked in
       assertTrue(booking.isCheckedIn());
               
       //Make sure the room is marked as occupied - End of UC 2.1.3
       assertFalse(rManager.getRooms().get(rr.getRoomId()).isFree());    
       
       //Add extra cost to booking
       receptionist.addExtraCost(bookingId, rr.getRoomId(), "laundry", 100);
       
       //Make sure that the extra cost was added
       assertTrue(rr.getRoomType().getCost()+100 == rr.getTotalCost());
       
       //Initiate the checkout to set currentCheckout in HotelCustomerProvides
       receptionist.initiateCheckout(bookingId);

       double bal = 0;
       
       //Create a card to use in the payment.
       try {
          se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires bankingAdmin = se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires.instance();
          
          // Add new credit card
          if (bankingAdmin.addCreditCard(this.cardHolderCCNumber, this.cardHolderCCV, this.cardHolderExMonth, this.cardHolderExYear, firstName, lastName)) {
              System.out.println("Successfully added credit card.");
          } else {
              System.out.println("Error while adding credit card. (Might already exist)");
          }
          
          bal = bankingAdmin.makeDeposit("00000003", "000", 11, 16, firstName, lastName, 1000);
          
          if (bal == -1 ) System.out.println("Could not makeDeposit");
          
      }catch (SOAPException e){
          e.printStackTrace();
      }
       
       //Pay the booking and check out
       assertTrue(receptionist.payDuringCheckout("00000003", "000", 11, 16, firstName, lastName));
       
       //Make sure the booking is payed
       assertTrue(booking.isPayed());
       
       //Make sure the booking was checked out out
       assertTrue(booking.isCheckedOut());
   }
	
}
